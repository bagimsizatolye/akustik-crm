<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CustomerConnection;
use Faker\Generator as Faker;

$factory->define(CustomerConnection::class, function (Faker $faker) {
    return [
        'description' => $faker->paragraph(rand(1, 3), true),
        'user_id' => rand(0, 2) > 0 ? App\Models\User::inRandomOrder()->first()->id : null,
        'company_id' => rand(0, 2) > 0 ? App\Models\Company::inRandomOrder()->first()->id: null,
        'customer_connection_type_id' => App\Models\CustomerConnectionType::inRandomOrder()->first()->id,
        'occurs_at' => $faker->dateTimeBetween('-1 year', '+1 year'),
        'ends_at' => $faker->dateTimeBetween('-1 year', '+1 year'),
    ];
});
