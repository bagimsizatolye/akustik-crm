<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Company;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {
    return [
        'name' => mb_convert_case($faker->company, MB_CASE_TITLE),
        'title' => mb_convert_case($faker->company, MB_CASE_TITLE),
        'email' => $faker->safeEmail,
        'phone' => $faker->e164PhoneNumber,
        'email_others' => array_merge([$faker->safeEmail, $faker->safeEmail]),
        'phone_others' => array_merge([$faker->e164PhoneNumber, $faker->e164PhoneNumber]),
        'website_url' => $faker->url,
        // 'notes' => implode("\n\n", $faker->paragraphs(rand(0, 5))),
        'address' => $faker->address,
        'billing_address' => $faker->address,
        'tax_number' => rand(111111111, 999999999),
        'tax_office' => $faker->city,
    ];
});
