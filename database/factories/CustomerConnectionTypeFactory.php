<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CustomerConnectionType;
use Faker\Generator as Faker;

$factory->define(CustomerConnectionType::class, function (Faker $faker) {
    return [
        'title' => $faker->words(rand(1, 3), true),
    ];
});
