<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'title' => $faker->words(rand(1, 6), true),
        'product_category_id' => App\Models\ProductCategory::inRandomOrder()->first()->id,
    ];
});
