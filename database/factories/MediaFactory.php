<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\Models\Media::class, function (Faker $faker) {
    $image1 = 'public/images/'.Str::random().'.jpg';
    $image2 = 'public/images/'.Str::random().'.jpg';
    $image1 = $faker->image(
        storage_path('app/public/images'),
        1600,
        1200,
        'abstract'
    );

    $image2 = $faker->image(
        storage_path('app/public/images'),
        600,
        400,
        'abstract'
    );

    return [
        'path' => str_replace(storage_path('app').'/', '', $image1),
        'path_thumbnail' => str_replace(storage_path('app').'/', '', $image2),
        'type_id' => 'image',
    ];
});

$factory->state(App\Models\Media::class, 'product', function (Faker $faker) {
    $image1 = $faker->image(
        storage_path('app/public/images/product'),
        1600,
        1200,
        'abstract'
    );

    $image2 = $faker->image(
        storage_path('app/public/images/product'),
        600,
        400,
        'abstract'
    );

    return [
        'path' => str_replace(storage_path('app').'/', '', $image1),
        'path_thumbnail' => str_replace(storage_path('app').'/', '', $image2),
    ];
});

$factory->state(App\Models\Media::class, 'work-category-icon', function (Faker $faker) {
    $image1 = $faker->image(
        storage_path('app/public/images'),
        480,
        480
    );

    $image2 = $faker->image(
        storage_path('app/public/images'),
        55,
        54
    );

    return [
        'path' => str_replace(storage_path('app').'/', '', $image1),
        'path_thumbnail' => str_replace(storage_path('app').'/', '', $image2),
    ];
});
