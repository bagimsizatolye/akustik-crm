<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 200);
            $table->string('title', 500)->nullable();
            $table->string('email', 500)->nullable();
            $table->string('phone', 500)->nullable();
            $table->json('email_others')->default('[]');
            $table->json('phone_others')->default('[]');
            $table->string('website_url', 500)->nullable();
            $table->string('address', 2000)->nullable();
            $table->string('billing_address', 2000)->nullable();
            $table->string('tax_number', 100)->nullable();
            $table->string('tax_office', 100)->nullable();
            $table->json('notes')->default('[]');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
