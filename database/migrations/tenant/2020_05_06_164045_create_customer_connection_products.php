<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerConnectionProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_connection_products', function (Blueprint $table) {
            $table->integer('customer_connection_id')->unsigned();
            $table->integer('product_id')->unsigned();

            $table->primary([
                'customer_connection_id',
                'product_id',
            ], 'primary_key');

            $table->foreign('customer_connection_id')
                ->references('id')->on('customer_connections')
                ->onUpdate('restrict')->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onUpdate('restrict')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_connection_products');
    }
}
