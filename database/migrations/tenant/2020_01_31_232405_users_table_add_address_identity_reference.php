<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersTableAddAddressIdentityReference extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('address', 2000)->nullable();
            $table->string('billing_address', 2000)->nullable();
            $table->string('tax_number', 100)->nullable();
            $table->string('tax_office', 100)->nullable();
            $table->string('government_id_number', 30)->nullable();
            $table->integer('reference_user_id')->nullable()->unsigned();

            $table->foreign('reference_user_id')
                ->references('id')->on('users')
                ->onUpdate('restrict')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_reference_user_id_foreign');
            $table->dropColumn([
                'address',
                'billing_address',
                'tax_number',
                'tax_office',
                'government_id_number',
                'reference_user_id',
            ]);
        });
    }
}
