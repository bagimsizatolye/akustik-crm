<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerConnectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_connections', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_connection_type_id')->unsigned();
            $table->integer('user_id')->nullable()->unsigned();
            $table->integer('company_id')->nullable()->unsigned();
            $table->string('description', 10000)->nullable();
            $table->decimal('price', 10, 2)->nullable();
            $table->json('extra')->default('{}');
            $table->dateTime('occurs_at')->nullable();
            $table->dateTime('ends_at')->nullable();
            $table->timestamps();

            $table->foreign('customer_connection_type_id')
                ->references('id')->on('customer_connection_types')
                ->onUpdate('restrict')->onDelete('restrict');

            $table->foreign('company_id')
                ->references('id')->on('companies')
                ->onUpdate('restrict')->onDelete('restrict');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('restrict')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_connections');
    }
}
