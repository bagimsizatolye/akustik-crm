<?php

use Illuminate\Database\Seeder;

use App\Models\CustomerConnectionType;

class CustomerConnectionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list = [
            [
                'id' => 1,
                'title' => 'Satış',
            ],
            [
                'id' => 2,
                'title' => 'Tamirat',
            ],
            [
                'id' => 3,
                'title' => 'Bakım',
            ],
            [
                'id' => 4,
                'title' => 'Hizmet',
            ],
            [
                'id' => 5,
                'title' => 'İletişim',
            ],
            [
                'id' => 6,
                'title' => 'Teklif',
            ],
            [
                'id' => 7,
                'title' => 'Toplantı',
            ],
            [
                'id' => 8,
                'title' => 'Not',
            ],
        ];

        foreach ($list as $item) {
            $found = CustomerConnectionType::find($item['id']);
            if ($found) {
                if ($found->title !== $item['title']) {
                    $found->delete();
                }
            }

            CustomerConnectionType::firstOrCreate($item);
        }
    }
}
