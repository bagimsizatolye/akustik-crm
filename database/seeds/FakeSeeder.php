<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FakeSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (!app()->environment('local')) {
            dd('Not in local environment!');
        }

        // DB::table('media')->truncate();
        // DB::table('products')->truncate();
        // DB::table('product_categories')->delete();
        // DB::table('users')->delete();
        // DB::table('companies')->truncate();
        // DB::table('customer_connection_types')->truncate();

        /**
         * @var Illuminate\Database\Eloquent\Collection;
         */
        // $productCategories = factory(App\Models\ProductCategory::class, rand(4, 8))
        //     ->create();
        // // ->each(function (App\Models\ProductCategory $category) {
        // //     $media = factory(App\Models\Media::class, 1)
        // //         ->states('work-category-icon')
        // //         ->create([
        // //             'related_to' => 'work-category-icon',
        // //             'related_id' => $category->id,
        // //         ]);
        // // });
        //
        // /**
        //  * @var Illuminate\Database\Eloquent\Collection;
        //  */
        // $products = factory(App\Models\Product::class, rand(5, 10))
        //     ->create()
        //     ->each(function (App\Models\Product $product) {
        //         $media = factory(App\Models\Media::class, rand(0, 5))
        //             ->states('product')
        //             ->create([
        //                 'related_to' => 'product',
        //                 'related_id' => $product->id,
        //             ]);
        //     });
        //
        // $adminUser = new App\Models\User();
        // $adminUser->email = 'admin@admin.com';
        // $adminUser->password = bcrypt('123456');
        // $adminUser->email_verified_at = now();
        // $adminUser->name = 'Bağımsız Admin';
        // $adminUser->save();
        //
        // $users = factory(App\Models\User::class, 100)
        //     ->create();
        //
        $companies = factory(App\Models\Company::class, rand(5, 10))
           ->create()
           ->each(function (App\Models\Company $company) {
               $media = factory(App\Models\Media::class, rand(0, 5))
                   ->states('product')
                   ->create([
                       'related_to' => App\Models\Company::class,
                       'related_id' => $company->id,
                   ]);
           });
        //
        // $customerConnectionTypes = factory(App\Models\CustomerConnectionType::class, 5)
        //    ->create();

        $customerConnections = factory(App\Models\CustomerConnection::class, 100)
           ->create()
           ->each(function (App\Models\CustomerConnection $model) {
               $media = factory(App\Models\Media::class, rand(0, 1))
                   ->states('product')
                   ->create([
                       'related_to' => App\Models\CustomerConnection::class,
                       'related_id' => $model->id,
                   ]);
           });
    }
}
