@extends('layouts.app')

@section('header')
  <h3>
    @if($customerConnection->id)
      {{ $type->title }}
    @else
      {{ $type ? __('Yeni :relation', ['relation' => $type->title]) : __('Yeni İlişki') }}
    @endif
  </h3>
@endsection

@section('content')
  <form
    method="post"
    accept-charset="UTF-8"
    enctype="multipart/form-data"
    class="border-bottom pb-3 mb-3"
    @if($customerConnection->id)
      action="{{ route('customer-connection.update', $customerConnection->id) }}"
    @else
      action="{{ route('customer-connection.store') }}"
    @endif
  >
    @csrf

    @if($customerConnection->id)
      <input type="hidden" name="_method" value="put">
    @endif

    @if(!empty($redirectUrl))
      <input type="hidden" name="redirect_url" value="{{ $redirectUrl }}">
    @endif

    <div class="form-group">
      <label for="customer_connection_type_id">{{ __('Tip') }}</label>

      @if(is_null($type))
        <select
          name="customer_connection_type_id"
          class="form-control"
          id="customer_connection_type_id"
          required
        >
          <option value=""></option>
          @foreach($connectionTypeList as $item)
            <option
              @if(((int)old('customer_connection_type_id') ?: $customerConnection->customer_connection_type_id) === $item->id)
                selected
              @endif
              value="{{ $item->id }}"
            >{{ $item->title }}</option>
          @endforeach
        </select>
      @else
        <input
          type="text"
          readonly
          class="form-control-plaintext"
          id="customer_connection_type_id"
          value="{{ $type->title }}"
        >
        <input type="hidden" name="customer_connection_type_id" value="{{ $type->id }}">
      @endif
    </div>

    <div class="row">
      <div class="col-12 col-md-6">
        <div class="form-group">
          <label for="company_id">{{ __('Müşteri (Kurum)') }}</label>

          @if(is_null($company))
            <select
              name="company_id"
              class="form-control select2-company"
              id="company_id"
              data-placeholder="@lang('Kurum seçebilirsiniz')"
            >
              @if(old('company_id'))
                <option selected value="{{ old('company_id') }}">{{ old('company_id') }}</option>
              @endif
            </select>
          @else
            <input
              type="text"
              readonly
              class="form-control-plaintext"
              id="company_id"
              value="{{ $company->name ?: $company->title }}"
            >
            <input type="hidden" name="company_id" value="{{ $company->id }}">
          @endif
        </div>
      </div>
      <div class="col-12 col-md-6">
        <div class="form-group">
          <label for="user_id">{{ __('Müşteri (Kişi)') }}</label>

          @if(is_null($user))
            <select
              name="user_id"
              class="form-control select2-user"
              id="user_id"
              @if(!is_null($user))
                data-company-id="{{ $user->id }}"
              @endif
              data-placeholder="@lang('Kişi seçebilirsiniz')"
            >
              @if(old('user_id'))
                <option selected value="{{ old('user_id') }}">{{ old('user_id') }}</option>
              @endif
            </select>
          @else
            <input
              type="text"
              readonly
              class="form-control-plaintext"
              id="user_id"
              value="{{ $user->name }}"
            >
            <input type="hidden" name="user_id" value="{{ $user->id }}">
          @endif
        </div>
      </div>
    </div>

    <div class="form-group">
      <label for="description">{{ __('Açıklama') }}</label>
      <textarea
        name="description"
        class="form-control"
        id="description"
        maxlength="10000"
      >{{ old('description') ?: $customerConnection->description }}</textarea>
    </div>

    <div class="form-group">
      <label for="price">{{ __('Ücret') }}</label>
      <input
        type="number"
        name="price"
        class="form-control"
        id="price"
        step="0.01"
        min="0"
        max="99999999"
        value="{{ old('price') ?: $customerConnection->price }}"
      >
    </div>

    <div class="row">
      <div class="col-12 col-md-6">
        <div class="form-group">
          <label for="occurs_at_date">{{ __('Gerçekleşme tarihi') }}</label>
          <input
            type="date"
            name="occurs_at_date"
            class="form-control"
            id="occurs_at_date"
            value="{{ old('occurs_at_date') ?: ($customerConnection->occurs_at ? $customerConnection->occurs_at->format('Y-m-d') : now()->format('Y-m-d')) }}"
          >
        </div>
      </div>
      <div class="col-12 col-md-6">
        <div class="form-group">
          <label for="occurs_at_time">{{ __('Saati') }}</label>
          <input
            type="time"
            name="occurs_at_time"
            class="form-control"
            id="occurs_at_time"
            step="60"
            value="{{ old('occurs_at_time') ?: ($customerConnection->occurs_at ? $customerConnection->occurs_at->format('H:i') : now()->format('H:i')) }}"
          >
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-12 col-md-6">
        <div class="form-group">
          <label for="ends_at_date">{{ __('Bitiş tarihi') }}</label>
          <input
            type="date"
            name="ends_at_date"
            class="form-control"
            id="ends_at_date"
            value="{{ old('ends_at_date') ?: ($customerConnection->ends_at ? $customerConnection->ends_at->format('Y-m-d') : '') }}"
          >
        </div>
      </div>
      <div class="col-12 col-md-6">
        <div class="form-group">
          <label for="ends_at_time">{{ __('Saati') }}</label>
          <input
            type="time"
            name="ends_at_time"
            class="form-control"
            id="ends_at_time"
            step="60"
            value="{{ old('ends_at_time') ?: ($customerConnection->ends_at ? $customerConnection->ends_at->format('H:i') : '') }}"
          >
        </div>
      </div>
    </div>

    <div class="form-group">
      <label for="">{{ __('Ürünler') }}</label>
      <select
        name="product_ids[]"
        class="form-control select2-product"
        id="product_ids"
        multiple
      >
        @if(old('product_ids'))
          @foreach (old('product_ids') as $productId)
            <option selected value="{{ $productId }}">{{ $productId }}</option>
            <?php // TODO: Fix the name of old input ?>
          @endforeach
        @elseif ($customerConnection->products->count())
          @foreach ($customerConnection->products as $product)
            <option selected value="{{ $product->id }}">{{ $product->title }}</option>
          @endforeach
        @else
          <option value=""></option>
        @endif
      </select>
    </di

    <div class="form-group">
      <label for="media">{{ __('Görseller') }}</label>
      <input
        class="form-control"
        id="media"
        name="media[]"
        type="file"
        accept="image/*"
        multiple
      >
      <p class="help-block">
        {{ __('Sınırsız ekleyebilirsiniz. "Kaydet"tiğinizde yüklenecek. Yüklediğiniz dosya boyutuna göre uzunca vakit alabilir.') }}
      </p>
    </div>

    <div class="text-right">
      <button type="submit" class="btn btn-outline-primary">{{ __('Kaydet') }}</button>
    </div>

  </form>

  @if($customerConnection->id and $customerConnection->media->count())
    <h3>{{ __('Mevcut Görseller') }}</h3>

    <gallery-edit-component
      gallery-json="{!! str_replace('"', '\'', json_encode($customerConnection->media->toArray(), JSON_HEX_APOS | JSON_HEX_QUOT)) !!}"
      csrf-token="{{ csrf_token() }}"
    ></gallery-edit-component>
  @endif
@endsection
