@extends('layouts.app')

@section('header')
  <div class="row">
    <div class="col-12 col-md-6 text-center text-md-left">
      <h3>{{ $company->name }}</h3>
    </div>
    <div class="col-12 col-md-6 text-right">
      <a class="btn btn-sm btn-outline-primary" href="{{ route('company.edit', $company->id) }}">
        @lang('Düzenle')
      </a>
    </div>
  </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-12 col-md-6">
      <p>
        @if(!empty($company->title))
          @lang('Unvan'): <strong>{{ $company->title }}</strong>
        @endif
      </p>
      <p>
        @if(!empty($company->phone))
          @lang('Telefon'): <strong><a href="tel:{{ $company->phone }}">{{ $company->phone }}</a></strong>
        @endif
      </p>
      <p>
        @if(!empty($company->email))
          @lang('E-posta'): <strong><a href="mailto:{{ $company->email }}">{{ $company->email }}</a></strong>
        @endif
      </p>
      <p>
        @if(!empty($company->website_url))
          @lang('İnternet sitesi'):
          <strong><a target="_blank" href="{{ $company->website_url }}">{{ str_limit($company->website_url, 30) }}</a></strong>
        @endif
      </p>
    </div>
    <div class="col-12 col-md-6">
      <p>
        @if(!empty($company->address))
          @lang('Adres'): <strong>{{ $company->address }}</strong>
        @endif
      </p>
      <p>
        @if(!empty($company->tax_number))
          @lang('Vergi Numarası'): <strong>{{ $company->tax_number }}</strong>
        @endif
      </p>
      <p>
        @if(!empty($company->tax_office))
          @lang('Vergi Dairesi'): <strong>{{ $company->tax_office }}</strong>
        @endif
      </p>
      <p>
        @if(!empty($company->billing_address))
          @lang('Fatura Adresi'): <strong>{{ $company->billing_address }}</strong>
        @endif
      </p>
      <p>
        @if($company->users->count())
          @lang('Kişiler'):
          @foreach ($company->users as $user)
            <a href="{{ route('user.show', $user->id) }}">
              <strong>{{ $user->name }}</strong>
            </a>
            @if(!$loop->last)
              ,
            @endif
          @endforeach
        @endif
      </p>
    </div>
  </div>

  <hr>

  <h3>@lang('Müşteri İlişkileri')</h3>

  <div class="timeline-filters">
    <form class="form-inline">
      <label class="m-1" for="type_id">
        Filtrele:
      </label>
      <select class="custom-select m-1" name="type_id" id="type_id">
        <option value="">{{ __('Tümü') }}</option>
        @foreach($connectionTypeList as $type)
          <option value="{{ $type->id }}">
            {{ $type->title }}
          </option>
        @endforeach
      </select>
    </form>
  </div>

  <ul class="timeline timeline-user">
    @foreach($company->connections as $connection)
      <li
        class="timeline-item"
        data-date="{{ format_datetime($connection->occurs_at) }}"
        data-type_id="{{ $connection->customer_connection_type_id }}"
      >
        <h3>{{ $connection->type->title }}</h3>

        <div class="row">
          <div class="col-12 col-lg-8">
            @if($connection->user)
              <p class="user">
                {{ $connection->user->name }}
              </p>
            @endif

            @if($connection->description)
              <p class="description">
                {{ $connection->description }}
              </p>
            @endif
          </div>
          <div class="col-12 col-lg-4">
            <div class="action-buttons">
              <a
                href="{{ route('customer-connection.edit', [
                  $connection->id,
                  'redirect_url' => request()->getRequestUri(),
                  ]) }}"
                class="btn btn-outline-info btn-sm my-1"
              >{{ __('Düzenle') }}</a>

              <form
                action="{{ route('customer-connection.destroy', $connection->id) }}"
                class="d-inline"
                method="post"
              >
                <input type="hidden" name="_method" value="delete">
                @csrf

                <button
                  type="submit"
                  class="btn btn-outline-danger btn-sm my-1"
                  data-ask="{{ __('Silmek istediğinden emin misin?') }}"
                >{{ __('Sil') }}</button>
              </form>
            </div>
          </div>
        </div>
      </li>
    @endforeach
  </ul>
@endsection
