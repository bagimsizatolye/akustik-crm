@extends('layouts.app')

@section('header')
  <h3>{{ $company->id ? $company->name : __('Yeni Kurum') }}</h3>
@endsection

@section('content')
  <form
    method="post"
    accept-charset="UTF-8"
    enctype="multipart/form-data"
    class="border-bottom pb-3 mb-3"
    @if($company->id)
      action="{{ route('company.update', $company->id) }}"
    @else
      action="{{ route('company.store') }}"
    @endif
  >
    @csrf

    @if($company->id)
      <input type="hidden" name="_method" value="put">
    @endif

    <div class="row">
      <div class="col-12 col-lg-6">
        <div class="form-group">
          <label for="name">{{ __('İsim') }}</label>
          <input
            type="text"
            name="name"
            class="form-control"
            id="name"
            maxlength="300"
            minlength="1"
            required
            value="{{ old('name') ?: $company->name }}"
          >
        </div>
      </div>
      <div class="col-12 col-lg-6">
        <div class="form-group">
          <label for="title">{{ __('Unvan') }}</label>
          <input
            type="text"
            name="title"
            class="form-control"
            id="title"
            maxlength="300"
            minlength="1"
            value="{{ old('title') ?: $company->title }}"
          >
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-12 col-lg-6">
        <div class="form-group">
          <label for="email">{{ __('E-posta') }}</label>
          <input
            type="email"
            name="email"
            class="form-control"
            id="email"
            maxlength="300"
            minlength="4"
            value="{{ old('email') ?: $company->email }}"
          >
        </div>
      </div>
      <div class="col-12 col-lg-6">
        <div class="form-group">
          <label for="phone">{{ __('Telefon') }}</label>
          <input
          type="tel"
          name="phone"
          class="form-control"
          id="phone"
          maxlength="300"
          minlength="5"
          value="{{ old('phone') ?: $company->phone }}"
          >
        </div>
      </div>
    </div>

    <div class="form-group">
      <label for="website_url">{{ __('İnternet Sitesi') }}</label>
      <input
        type="url"
        name="website_url"
        class="form-control"
        id="website_url"
        maxlength="500"
        minlength="10"
        value="{{ old('website_url') ?: $company->website_url }}"
      >
    </div>

    <div class="row">
      <div class="col-12 col-lg-6">
        <div class="form-group">
          <label for="address">{{ __('Adres') }}</label>
          <textarea
            name="address"
            class="form-control"
            id="address"
            maxlength="2000"
          >{{ old('address') ?: $company->address }}</textarea>
        </div>
      </div>
      <div class="col-12 col-lg-6">
        <div class="form-group">
          <label for="billing_address">{{ __('Fatura Adresi') }}</label>
          <textarea
            name="billing_address"
            class="form-control"
            id="billing_address"
            maxlength="2000"
          >{{ old('billing_address') ?: $company->billing_address }}</textarea>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-12 col-lg-6">
        <div class="form-group">
          <label for="tax_number">{{ __('Vergi Numarası') }}</label>
          <input
            type="text"
            name="tax_number"
            class="form-control"
            id="tax_number"
            maxlength="100"
            value="{{ old('tax_number') ?: $company->tax_number }}"
          >
        </div>
      </div>
      <div class="col-12 col-lg-6">
        <div class="form-group">
          <label for="tax_office">{{ __('Vergi Dairesi') }}</label>
          <input
            type="text"
            name="tax_office"
            class="form-control"
            id="tax_office"
            maxlength="100"
            value="{{ old('tax_office') ?: $company->tax_office }}"
          >
        </div>
      </div>
    </div>

    <div class="form-group">
      <label for="user_ids">{{ __('Kişiler') }}</label>
      <select
        name="user_ids[]"
        class="form-control select2-user"
        id="user_ids"
        multiple
      >
        @if(old('user_ids'))
          @foreach (old('user_ids') as $userId)
            <option selected value="{{ $userId }}">{{ $userId }}</option>
          @endforeach
        @elseif ($company->users->count())
          @foreach ($company->users as $user)
            <option selected value="{{ $user->id }}">{{ $user->name }}</option>
          @endforeach
        @else
          <option value=""></option>
        @endif
      </select>
    </div>

    <div class="form-group">
      <label for="media">{{ __('Görseller') }}</label>
      <input
        class="form-control"
        id="media"
        name="media[]"
        type="file"
        accept="image/*"
        multiple
      >
      <p class="help-block">
        {{ __('Sınırsız ekleyebilirsin. "Kaydet"tiğinde yüklenecek. Vakit alabilir, biraz sabır.') }}
      </p>
    </div>

    <div class="text-right">
      <button type="submit" class="btn btn-outline-primary">{{ __('Kaydet') }}</button>
    </div>

  </form>

  @if($company->id and $company->media->count())
    <h3>{{ __('Mevcut Görseller') }}</h3>

    <gallery-edit-component
      gallery-json="{!! str_replace('"', '\'', json_encode($company->media->toArray(), JSON_HEX_APOS | JSON_HEX_QUOT)) !!}"
      csrf-token="{{ csrf_token() }}"
    ></gallery-edit-component>
  @endif
@endsection
