@extends('layouts.app')

@section('header')
  <h3>{{ __('Aylık Gelir') }}</h3>
@endsection

@section('content')
  <canvas
    id="myChart"
    width="600"
    height="300"
    data-report-data="{{ json_encode($reportData, JSON_HEX_QUOT) }}"
  ></canvas>
@endsection
