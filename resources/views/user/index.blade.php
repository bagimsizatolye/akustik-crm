@extends('layouts.app')

@section('header')
  <div class="row">
    <div class="col-12 col-md-8">
      <h3 class="text-center text-md-left">{{ __('Kişiler') }}</h3>
    </div>
    <div class="col-12 col-md-4 text-xs-center text-md-right">
      <a class="btn btn-dark btn-sm" href="{{ route('user.create') }}">
        {{ __('Yeni Kişi Ekle') }}
      </a>
    </div>
  </div>
@endsection

@section('content')
  <div class="list-filters mb-3 mt-3">
    <form
      action="{{ route('user.index') }}"
      method="get"
    >
      <div class="row">
        <div class="col-12 col-md-6 col-lg-3">
          <div class="form-group">
            <label for="query">{{ __('Ara') }}</label>
            <input
              type="text"
              name="query"
              class="form-control"
              id="query"
              maxlength="300"
              minlength="1"
              value="{{ request('query') }}"
              placeholder="{{ __('İsim, e-posta veya telefon') }}"
            >
          </div>
        </div>
        <div class="col-12 col-md-6 col-lg-3 align-self-end mb-3">
          <button type="submit" class="btn btn-outline-secondary">
            {{ __('Filtrele') }}
          </button>
        </div>
      </div>
    </form>
  </div>

  <table class="table table-striped">
    <thead>
      <tr>
        <th>{{ __('İsim') }}</th>
        <th>{{ __('E-posta') }}</th>
        <th>{{ __('Telefon') }}</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach($list as $item)
        <tr>
          <td>
            <a href="{{ route('user.show', $item->id) }}">
              @if(!empty($item->mediaFeatured))
                <img src="{{ $item->mediaFeatured->url_thumb }}">
              @endif

              {{ $item->name }}
            </a>
          </td>
          <td>
            @if(!empty($item->email))
              <a href="mailto:{{ $item->email }}">{{ $item->email }}</a>
            @endif
          </td>
          <td>
            @if(!empty($item->phone))
              <a href="tel:{{ $item->phone }}">{{ $item->phone }}</a>
            @endif
          </td>
          <td class="text-right">
            <a
              class="btn btn-outline-info btn-sm"
              href="{{ route('user.edit', $item->id) }}"
            >{{ __('Düzenle') }}</a>

            @if($item->id !== auth()->id())
              <form
                action="{{ route('user.destroy', $item->id) }}"
                class="d-inline"
                method="post"
              >
                <input type="hidden" name="_method" value="delete">
                @csrf

                <button
                  type="submit"
                  class="btn btn-outline-danger btn-sm"
                  data-ask="{{ __('Silmek istediğinden emin misin?') }}"
                >{{ __('Sil') }}</button>
              </form>
            @endif
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>

  @if($list->count() === 0)
    <p class="text-center">
      {{ __('Hiçbir şey eklenmemiş.') }}
    </p>
  @endif

  {!! $list->render() !!}
@endsection
