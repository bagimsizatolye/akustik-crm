@extends('layouts.app')

@section('header')
  <h3>{{ $user->id ? $user->name : __('Yeni Kişi') }}</h3>
@endsection

@section('content')
  <form
    method="post"
    accept-charset="UTF-8"
    enctype="multipart/form-data"
    class="border-bottom pb-3 mb-3"
    @if($user->id)
      action="{{ route('user.update', $user->id) }}"
    @else
      action="{{ route('user.store') }}"
    @endif
  >
    @csrf

    @if($user->id)
      <input type="hidden" name="_method" value="put">
    @endif

    <div class="form-group">
      <label for="name">{{ __('İsim') }}</label>
      <input
        type="text"
        name="name"
        class="form-control"
        id="name"
        maxlength="300"
        minlength="1"
        required
        value="{{ old('name') ?: $user->name }}"
      >
    </div>

    <div class="row">
      <div class="col-12 col-lg-6">
        <div class="form-group">
          <label for="email">{{ __('E-posta') }}</label>
          <input
            type="email"
            name="email"
            class="form-control"
            id="email"
            maxlength="300"
            minlength="4"
            value="{{ old('email') ?: $user->email }}"
          >
        </div>
      </div>
      <div class="col-12 col-lg-6">
        <div class="form-group">
          <label for="phone">{{ __('Telefon') }}</label>
          <input
          type="tel"
          name="phone"
          class="form-control"
          id="phone"
          maxlength="300"
          minlength="5"
          value="{{ old('phone') ?: $user->phone }}"
          >
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-12 col-lg-6">
        <div class="form-group">
          <label for="address">{{ __('Adres') }}</label>
          <textarea
            name="address"
            class="form-control"
            id="address"
            maxlength="2000"
          >{{ old('address') ?: $user->address }}</textarea>
        </div>
      </div>
      <div class="col-12 col-lg-6">
        <div class="form-group">
          <label for="billing_address">{{ __('Fatura Adresi') }}</label>
          <textarea
            name="billing_address"
            class="form-control"
            id="billing_address"
            maxlength="2000"
          >{{ old('billing_address') ?: $user->billing_address }}</textarea>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-12 col-lg-6">
        <div class="form-group">
          <label for="tax_number">{{ __('Vergi Numarası') }}</label>
          <input
            type="text"
            name="tax_number"
            class="form-control"
            id="tax_number"
            maxlength="100"
            value="{{ old('tax_number') ?: $user->tax_number }}"
          >
        </div>
      </div>
      <div class="col-12 col-lg-6">
        <div class="form-group">
          <label for="tax_office">{{ __('Vergi Dairesi') }}</label>
          <input
            type="text"
            name="tax_office"
            class="form-control"
            id="tax_office"
            maxlength="100"
            value="{{ old('tax_office') ?: $user->tax_office }}"
          >
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-12 col-lg-6">
        <div class="form-group">
          <label for="government_id_number">{{ __('TC Kimlik No') }}</label>
          <input
            type="text"
            name="government_id_number"
            class="form-control"
            id="government_id_number"
            maxlength="50"
            value="{{ old('government_id_number') ?: $user->government_id_number }}"
          >
        </div>
      </div>
      <div class="col-12 col-lg-6">
        <div class="form-group">
          <label for="reference_user_id">{{ __('Referans kişi') }}</label>
          <select
            name="reference_user_id"
            class="form-control select2-user"
            id="reference_user_id"
            multiple
          >
            @if(old('reference_user_id'))
              <option selected value="{{ old('reference_user_id') }}">{{ old('reference_user_id') }}</option>
            @elseif ($user->reference_user_id)
              <option selected value="{{ $user->referenceUser->id }}">{{ $user->referenceUser->name }}</option>
            @else
              <option value=""></option>
            @endif
          </select>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-12">
        <div class="form-group">
          <label for="company_ids">{{ __('Kurumlar') }}</label>
          <select
            name="company_ids[]"
            class="form-control select2-company"
            id="company_ids"
            multiple
          >
            @if(old('company_ids'))
              @foreach (old('company_ids') as $companyId)
                <option selected value="{{ $companyId }}">{{ $companyId }}</option>
              @endforeach
            @elseif ($user->companies->count())
              @foreach ($user->companies as $company)
                <option selected value="{{ $company->id }}">{{ $company->name ?: $company->title }}</option>
              @endforeach
            @else
              <option value=""></option>
            @endif
          </select>
        </div>
      </div>
    </div>

    <div class="form-group">
      <label for="media">{{ __('Görseller') }}</label>
      <input
        class="form-control"
        id="media"
        name="media[]"
        type="file"
        accept="image/*"
        multiple
      >
      <p class="help-block">
        {{ __('Sınırsız ekleyebilirsin. "Kaydet"tiğinde yüklenecek. Vakit alabilir, biraz sabır.') }}
      </p>
    </div>

    <div class="text-right">
      <button type="submit" class="btn btn-outline-primary">{{ __('Kaydet') }}</button>
    </div>

  </form>

  @if($user->id and $user->media->count())
    <h3>{{ __('Mevcut Görseller') }}</h3>

    <gallery-edit-component
      gallery-json="{!! str_replace('"', '\'', json_encode($user->media->toArray(), JSON_HEX_APOS | JSON_HEX_QUOT)) !!}"
      csrf-token="{{ csrf_token() }}"
    ></gallery-edit-component>
  @endif
@endsection
