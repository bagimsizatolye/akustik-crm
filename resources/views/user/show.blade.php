@extends('layouts.app')

@section('header')
  <div class="row">
    <div class="col-12 col-md-6 text-center text-md-left">
      <h3>{{ $user->name }}</h3>
    </div>
    <div class="col-12 col-md-6 text-right">
      <a class="btn btn-sm btn-outline-primary" href="{{ route('user.edit', $user->id) }}">
        @lang('Düzenle')
      </a>
    </div>
  </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-12 col-md-6">
      <p>
        @if(!empty($user->name))
          @lang('İsim'): <strong>{{ $user->name }}</strong>
        @endif
      </p>
      <p>
        @if(!empty($user->phone))
          @lang('Telefon'): <strong><a href="tel:{{ $user->phone }}">{{ $user->phone }}</a></strong>
        @endif
      </p>
      <p>
        @if(!empty($user->email))
          @lang('E-posta'): <strong><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></strong>
        @endif
      </p>
      <p>
        @if(!empty($user->reference_user_id))
          @lang('Referans'):
          <strong><a href="{{ route('user.show', $user->reference_user_id) }}">{{ $user->referenceUser->name }}</a></strong>
        @endif
      </p>
      <p>
        @if(!empty($user->government_id_number))
          @lang('TC Kimlik No'): <strong>{{ $user->government_id_number }}</strong>
        @endif
      </p>
    </div>
    <div class="col-12 col-md-6">
      <p>
        @if(!empty($user->address))
          @lang('Adres'): <strong>{{ $user->address }}</strong>
        @endif
      </p>
      <p>
        @if(!empty($user->tax_number))
          @lang('Vergi Numarası'): <strong>{{ $user->tax_number }}</strong>
        @endif
      </p>
      <p>
        @if(!empty($user->tax_office))
          @lang('Vergi Dairesi'): <strong>{{ $user->tax_office }}</strong>
        @endif
      </p>
      <p>
        @if(!empty($user->billing_address))
          @lang('Fatura Adresi'): <strong>{{ $user->billing_address }}</strong>
        @endif
      </p>
      <p>
        @if($user->companies->count())
          @lang('Kurumlar'):
          @foreach ($user->companies as $company)
            <a href="{{ route('company.show', $company->id) }}">
              <strong>{{ $company->name ?: $company->title }}</strong>
            </a>
            @if(!$loop->last)
              ,
            @endif
          @endforeach
        @endif
      </p>
    </div>
  </div>

  <hr>

  <h3>@lang('Müşteri İlişkileri')</h3>

  <div class="timeline-filters">
    <form class="form-inline">
      <label class="m-1" for="type_id">
        Filtrele:
      </label>
      <select class="custom-select m-1" name="type_id" id="type_id">
        <option value="">{{ __('Tümü') }}</option>
        @foreach($connectionTypeList as $type)
          <option value="{{ $type->id }}">
            {{ $type->title }}
          </option>
        @endforeach
      </select>
    </form>
  </div>

  <ul class="timeline timeline-user">
    @foreach($user->connections as $connection)
      <li
        class="timeline-item"
        data-date="{{ format_datetime($connection->occurs_at) }}"
        data-type_id="{{ $connection->customer_connection_type_id }}"
      >
        <h3>{{ $connection->type->title }}</h3>

        <div class="row">
          <div class="col-12 col-lg-8">
            @if($connection->company)
              <p class="company">
                {{ $connection->company->name ?: $connection->company->title }}
              </p>
            @endif

            @if($connection->description)
              <p class="description">
                {{ $connection->description }}
              </p>
            @endif
          </div>
          <div class="col-12 col-lg-4">
            <div class="action-buttons">
              <a
                href="{{ route('customer-connection.edit', [
                  $connection->id,
                  'redirect_url' => request()->getRequestUri(),
                  ]) }}"
                class="btn btn-outline-info btn-sm my-1"
              >{{ __('Düzenle') }}</a>

              <form
                action="{{ route('customer-connection.destroy', $connection->id) }}"
                class="d-inline"
                method="post"
              >
                <input type="hidden" name="_method" value="delete">
                @csrf

                <button
                  type="submit"
                  class="btn btn-outline-danger btn-sm my-1"
                  data-ask="{{ __('Silmek istediğinden emin misin?') }}"
                >{{ __('Sil') }}</button>
              </form>
            </div>
          </div>
        </div>
      </li>
    @endforeach
  </ul>
@endsection
