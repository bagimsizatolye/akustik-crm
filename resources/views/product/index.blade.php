@extends('layouts.app')

@section('header')
  <div class="row">
    <div class="col-12 col-md-8">
      <h3 class="text-center text-md-left">{{ __('Ürünler') }}</h3>
    </div>
    <div class="col-12 col-md-4 text-xs-center text-md-right">
      <a class="btn btn-dark btn-sm" href="{{ route('product.create') }}">
        {{ __('Yeni Ürün Ekle') }}
      </a>
    </div>
  </div>
@endsection

@section('content')
  <div class="list-filters mb-3 mt-3">
    <form
      action="{{ route('product.index') }}"
      method="get"
    >
      <div class="row">
        <div class="col-12 col-md-6 col-lg-3">
          <div class="form-group">
            <label for="title">{{ __('İsim') }}</label>
            <input
              type="text"
              name="title"
              class="form-control"
              id="title"
              maxlength="150"
              minlength="1"
              value="{{ request('title') }}"
            >
          </div>
        </div>
        <div class="col-12 col-md-6 col-lg-3">
          <div class="form-group">
            <label for="product_category_id">{{ __('Kategori') }}</label>
            <select
              name="product_category_id"
              class="form-control"
              id="product_category_id"
            >
            <option></option>
            @foreach($categoryList as $item)
              <option
              @if(((int)request('product_category_id')) === $item->id)
                selected
              @endif
              value="{{ $item->id }}"
              >{{ $item->title }}</option>
            @endforeach
            </select>
          </div>
        </div>
        <div class="col-12 col-md-6 col-lg-3 align-self-end mb-3">
          <button type="submit" class="btn btn-outline-secondary">
            {{ __('Filtrele') }}
          </button>
        </div>
      </div>
    </form>
  </div>

  <table class="table table-striped">
    <thead>
      <tr>
        <th>{{ __('Ürün') }}</th>
        <th>{{ __('Kategori') }}</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach($list as $item)
        <tr>
          <td>
            @if(!empty($item->mediaFeatured))
              <img src="{{ $item->mediaFeatured->url_thumb }}">
            @endif

            {{ $item->title }}
          </td>
          <td>
            {{ $item->productCategory->title }}
          </td>
          <td class="text-right">
            <a
              class="btn btn-outline-info btn-sm"
              href="{{ route('product.edit', $item->id) }}"
            >{{ __('Düzenle') }}</a>

            <form
              action="{{ route('product.destroy', $item->id) }}"
              class="d-inline"
              method="post"
            >
              <input type="hidden" name="_method" value="delete">
              @csrf

              <button
                type="submit"
                class="btn btn-outline-danger btn-sm"
                data-ask="{{ __('Silmek istediğinden emin misin?') }}"
              >{{ __('Sil') }}</button>
            </form>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>

  @if($list->count() === 0)
    <p class="text-center">
      {{ __('Hiçbir şey eklenmemiş.') }}
    </p>
  @endif

  {!! $list->render() !!}
@endsection
