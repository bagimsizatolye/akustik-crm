@extends('layouts.app')

@section('header')
  <h3>{{ $product->id ? $product->title : __('Yeni Ürün') }}</h3>
@endsection

@section('content')
  <form
    method="post"
    accept-charset="UTF-8"
    enctype="multipart/form-data"
    class="border-bottom pb-3 mb-3"
    @if($product->id)
      action="{{ route('product.update', $product->id) }}"
    @else
      action="{{ route('product.store') }}"
    @endif
  >
    @csrf

    @if($product->id)
      <input type="hidden" name="_method" value="put">
    @endif

    <div class="form-group">
      <label for="title">{{ __('İsim') }}</label>
      <input
        type="text"
        name="title"
        class="form-control"
        id="title"
        maxlength="150"
        minlength="1"
        required
        value="{{ old('title') ?: $product->title }}"
      >
    </div>

    <div class="form-group">
      <label for="product_category_id">{{ __('Kategori') }}</label>
      <select
        name="product_category_id"
        class="form-control"
        id="product_category_id"
        required
      >
        <option value=""></option>
        @foreach($categoryList as $item)
          <option
            @if(((int)old('product_category_id') ?: $product->product_category_id) === $item->id)
              selected
            @endif
            value="{{ $item->id }}"
          >{{ $item->title }}</option>
        @endforeach
      </select>
      <p>
        <product-category-modal></product-category-modal>
      </p>
    </div>

    <div class="form-group">
      <label for="price_amount">{{ __('Fiyat') }}</label>
      <input
        type="number"
        name="price_amount"
        class="form-control"
        id="price_amount"
        step="0.01"
        min="0"
        max="99999999"
        value="{{ old('price_amount') ?: ($product->currentPrice ? $product->currentPrice->amount : '') }}"
      >
    </div>

    <div class="form-group">
      <label for="media">{{ __('Görseller') }}</label>
      <input
        class="form-control"
        id="media"
        name="media[]"
        type="file"
        accept="image/*"
        multiple
      >
      <p class="help-block">
        {{ __('Sınırsız ekleyebilirsin. "Kaydet"tiğinde yüklenecek. Vakit alabilir, biraz sabır.') }}
      </p>
    </div>

    <div class="text-right">
      <button type="submit" class="btn btn-outline-primary">{{ __('Kaydet') }}</button>
    </div>

  </form>

  @if($product->id and $product->media->count())
    <h3>{{ __('Mevcut Görseller') }}</h3>

    <gallery-edit-component
      gallery-json="{!! str_replace('"', '\'', json_encode($product->media->toArray(), JSON_HEX_APOS | JSON_HEX_QUOT)) !!}"
      csrf-token="{{ csrf_token() }}"
    ></gallery-edit-component>
  @endif
@endsection

@section('modals')
  <template>
    <modal name="product-category" :adaptive="true">
      <button class="modal-close" @click="$modal.hide('product-category')">
        ❌
      </button>

      @include('layouts.modal', [
        'header' => 'Yeni Kategori Ekle',
        'content' => 'forms.product-category',
      ])
    </modal>
  </template>
@endsection
