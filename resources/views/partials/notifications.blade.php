@if(session()->has('success'))
  <div class="alert alert-success alert-dismissible fade show" role="alert">
    {{ session('success') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@endif

@if(isset($errors) and is_string($errors))
  <div class="alert alert-danger alert-dismissible fade show" role="alert">
    {{ $errors }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@elseif(isset($errors) and $errors->count())
  <div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    <ul>
      @foreach($errors->toArray() as $error)
        <li>{{{ $error[0] }}}</li>
      @endforeach
    </ul>
  </div>
@endif
