<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  @auth
    <meta name="api-token" content="{{ auth()->user()->api_token }}">
  @endauth

  <base href="/">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link rel="icon" type="image/png" href="{{ asset('images/favicon.png') }}">
  <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}">
</head>

<body>
  <div id="app">
    @auth
      <nav class="navbar-admin">
        <div class="p-4">
          <a href="{{ route('home') }}">
            <!-- <img src="img/logo-footer.svg" alt="Bağımsız Atölye Logo" class="img-fluid logo"> -->
            Akustik CRM
          </a>
        </div>
        <button class="hamburger hamburger--arrowturn d-lg-none" type="button" role="button">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </button>
        <ul>
          <li class="{{ request()->routeIs('product.*') ? 'active' : '' }}">
            <a href="{{ route('product.index') }}">{{ __('Ürünler') }}</a>
          </li>
          <li class="{{ request()->routeIs('product-category.*') ? 'active' : '' }}">
            <a href="{{ route('product-category.index') }}">{{ __('Ürün Kategorileri') }}</a>
          </li>
          <li class="has-submenu {{ (request()->routeIs('user.*') or request()->routeIs('company.*')) ? 'active' : '' }}">
            <a class="submenu-toggler" href="#">{{ __('Rehber') }}</a>

            <ul class="submenu">
              <li class="{{ request()->routeIs('user.*') ? 'active' : '' }}">
                <a href="{{ route('user.index') }}">{{ __('Kişiler') }}</a>
              </li>
              <li class="{{ request()->routeIs('company.*') ? 'active' : '' }}">
                <a href="{{ route('company.index') }}">{{ __('Kurumlar') }}</a>
              </li>
            </ul>
          </li>
          <li class="has-submenu {{ (request()->routeIs('customer-connection.*')) ? 'active' : '' }}">
            <a class="submenu-toggler" href="#">{{ __('Müşteri İlişkileri') }}</a>

            <ul class="submenu">
              <li class="{{ request()->routeIs('customer-connection.index') ? 'active' : '' }}">
                <a href="{{ route('customer-connection.index') }}">{{ __('Tümü') }}</a>
              </li>
            </ul>
          </li>
          <li class="has-submenu {{ request()->routeIs('reports.*') ? 'active' : '' }}">
            <a class="submenu-toggler" href="#">{{ __('Raporlar') }}</a>

            <ul class="submenu">
              <li class="{{ request()->routeIs('reports.monthly-income') ? 'active' : '' }}">
                <a href="{{ route('reports.monthly-income') }}">{{ __('Aylık Gelir') }}</a>
              </li>
            </ul>
          </li>
          <li>
            <button form="logoutForm" type="submit">{{ __('Çıkış') }}</a>
          </li>
        </ul>

        <form id="logoutForm" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
        </form>
      </nav>
    @endauth
    <!-- <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
      <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
          {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">

          </ul>

          <ul class="navbar-nav ml-auto">
            @guest
            <li class="nav-item">
              <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            @if (Route::has('register'))
            <li class="nav-item">
              <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
            </li>
            @endif
            @else
            <li class="nav-item dropdown">
              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                {{ Auth::user()->name }} <span class="caret"></span>
              </a>

              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                  {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                </form>
              </div>
            </li>
            @endguest
          </ul>
        </div>
      </div>
    </nav> -->

    <main class="p-3">
      <header class="mb-3 px-3 px-lg-0">
        @yield('header')
      </header>

      @include('partials.notifications')

      @yield('content')
    </main>

    <div class="modals-container">
      @yield('modals')
    </div>
  </div>

  @if(config('services.matomo.status'))
    <!-- Matomo -->
    <script type="text/javascript">
      var _paq = window._paq || [];
      /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
      _paq.push(["setDomains", ["{{ config('services.matomo.domains') }}"]]);
      _paq.push(['trackPageView']);
      _paq.push(['enableLinkTracking']);
      (function() {
        var u="{{ config('services.matomo.host') }}";
        _paq.push(['setTrackerUrl', u+'matomo.php']);
        _paq.push(['setSiteId', '{{ config('services.matomo.site_id') }}']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
      })();
    </script>
    <!-- End Matomo Code -->
  @endif

  <script>
    window.apiRoutes = {};
    window.apiRoutes['api.company.index'] = '{{ route('api.company.index') }}';
    window.apiRoutes['api.user.index'] = '{{ route('api.user.index') }}';
    window.apiRoutes['api.product.index'] = '{{ route('api.product.index') }}';
  </script>
</body>

</html>
