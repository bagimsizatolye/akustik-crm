@isset($header)
  <div class="modal-header">
    <h3>{{ $header }}</h3>
  </div>
@endisset

<div class="modal-content">
  @include($content)
</div>
