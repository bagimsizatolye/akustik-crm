@extends('layouts.app')

@section('header')
  <h3>{{ $productCategory->id ? $productCategory->title : __('Yeni Ürün Kategorisi') }}</h3>
@endsection

@section('content')
  @include('forms.product-category', ['productCategory' => $productCategory])
@endsection
