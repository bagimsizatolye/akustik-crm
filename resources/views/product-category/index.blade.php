@extends('layouts.app')

@section('header')
  <div class="row">
    <div class="col-12 col-md-8">
      <h3>{{ __('Ürün Kategorileri') }}</h3>
    </div>
    <div class="col-12 col-md-4 text-xs-center text-md-right">
      <a class="btn btn-dark btn-sm" href="{{ route('product-category.create') }}">
        {{ __('Yeni Ürün Kategorisi Ekle') }}
      </a>
    </div>
  </div>
@endsection

@section('content')
  <table class="table table-striped">
    <thead>
      <tr>
        <th>{{ __('Başlık') }}</th>
        <th>{{ __('Ürün Sayısı') }}</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach($list as $item)
        <tr>
          <td>
            {{ $item->title }}
          </td>
          <td>
            <!--  -->
          </td>
          <td class="text-right">
            <a
              class="btn btn-outline-info btn-sm"
              href="{{ route('product-category.edit', $item->id) }}"
            >{{ __('Düzenle') }}</a>

            <form
              action="{{ route('product-category.destroy', $item->id) }}"
              class="d-inline"
              method="post"
            >
              <input type="hidden" name="_method" value="delete">
              @csrf

              <button
                type="submit"
                class="btn btn-outline-danger btn-sm"
                data-ask="{{ __('Silmek istediğinden emin misin?') }}"
              >{{ __('Sil') }}</button>
            </form>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>

  @if($list->count() === 0)
    <p class="text-center">
      {{ __('Hiçbir şey eklenmemiş.') }}
    </p>
  @endif

  {!! $list->render() !!}
@endsection
