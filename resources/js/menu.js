$(() => {
  $('.has-submenu .submenu-toggler').on('click', (event) => {
    event.preventDefault();
    const submenuElement = $(event.target).parents('.has-submenu');

    const height = submenuElement.find('.submenu').height();

    if (!submenuElement.hasClass('active')) {
      submenuElement.find('.submenu').css({ height: '0px' });
      console.log(height);
      submenuElement.addClass('active');
      setTimeout(() => {
        submenuElement.find('.submenu').css({ height: height + 'px' });
      });
    } else {
      submenuElement.find('.submenu').css({ height: '0' });
      setTimeout(() => {
        submenuElement.removeClass('active');
        submenuElement.find('.submenu').css({ height: '' });
      }, 500);
    }
  });

  $('nav button.hamburger').on('click', (event) => {
    event.preventDefault();

    const element = $('nav button.hamburger');

    element.toggleClass('is-active');
    $('nav').toggleClass('is-active');
  });
});