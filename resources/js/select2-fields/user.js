$(function() {
  setTimeout(() => {
    $('.select2-user').each((k, v) => {
      v = $(v);
      const companyId = v.data('company-id') || null;

      v.select2({
        multiple: v.prop('multiple'),
        allowClear: !v.prop('required'),
        delay: 250,
        theme: 'bootstrap4',
        ajax: {
          url: window.apiRoutes['api.user.index'],
          method: 'get',
          dataType: 'json',
          data: (params) => {
            const data = {
              query: params.term,
              page: params.page || 1,
            };

            if (companyId !== null) {
              data.company_id = companyId;
            }

            return data;
          },
          processResults: function(response, params) {
            params.page = params.page || 1;

            const results = [];

            for (const item of response.data) {
              results.push({
                id: item.id,
                text: item.name,
              });
            }

            return {
              results: results,
              pagination: {
                more: (params.page * response.per_page) < response.total
              }
            };
          }
        },
      });
    });
  }, 100);
});