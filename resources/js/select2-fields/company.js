$(function() {
  setTimeout(() => {
    $('.select2-company').each((k, v) => {
      v = $(v);
      v.select2({
        multiple: v.prop('multiple'),
        allowClear: !v.prop('required'),
        delay: 250,
        theme: 'bootstrap4',
        ajax: {
          url: window.apiRoutes['api.company.index'],
          method: 'get',
          dataType: 'json',
          data: (params) => {
            return {
              query: params.term,
              page: params.page || 1,
            };
          },
          processResults: function(response, params) {
            params.page = params.page || 1;

            const results = [];

            for (const item of response.data) {
              results.push({
                id: item.id,
                text: item.name || item.title,
              });
            }

            return {
              results: results,
              pagination: {
                more: (params.page * response.per_page) < response.total
              }
            };
          }
        },
      });
    });
  }, 100);
});