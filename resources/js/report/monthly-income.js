import Chart from 'chart.js';

$(() => {
  if (!document.getElementById('myChart')) {
    return;
  }

  const ctx = document.getElementById('myChart').getContext('2d');

  const reportData = $('#myChart').data('report-data')
  const data = [];
  for (const i in reportData) {
    const item = reportData[i];
    data.push({
      x: new Date(item.occurs_year, item.occurs_month - 1),
      y: item.total_price || 0,
    });
  }

  const myChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
      datasets: [{
        label: 'Toplam gelir',
        data: data,
        backgroundColor: [
          '#e3342f',
        ],
        borderColor: [
          '#000000',
        ],
        borderWidth: 1
      }]
    },
    options: {
      scales: {
        xAxes: [{
          type: 'time',
          distribution: 'series',
          time: {
            unit: 'month',
            displayFormats: {
              month: 'MMM YYYY',
            },
            tooltipFormat: 'MMM YYYY',
          }
        }],
      },
    },
  });
});
