$(() => {
  const timeline = document.querySelector('.timeline');

  if(!timeline) return;

  const timelineFilter = document.querySelector('.timeline-filters select');

  if(timelineFilter) {
    timelineFilter.onchange = (event) => {
      timeline.querySelectorAll('li').forEach((element) => {
        element.classList.remove('hide');
      });
      if(timelineFilter.value) {
        timeline.querySelectorAll('li').forEach((element) => {
          if(element.dataset.type_id != timelineFilter.value) {
            element.classList.add('hide');
          }
        });
      }
    };
  }
});
