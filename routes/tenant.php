<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use App\Models\Tenant;

Auth::routes();

Route::get('', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function () {
    Route::resource('product-category', 'ProductCategoryController');
    Route::resource('product', 'ProductController');
    Route::resource('media', 'MediaController');
    Route::resource('user', 'UserController');
    Route::resource('company', 'CompanyController');
    Route::resource('customer-connection', 'CustomerConnectionController');

    Route::get('reports/monthly-income', 'ReportController@getMonthlyIncome')
        ->name('reports.monthly-income');
});
