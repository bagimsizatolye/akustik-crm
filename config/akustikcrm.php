<?php

return [
    /**
     * Define how a tenant will be identified.
     * Available values: path, domain
     */
    'tenancy_http_identification' => env('TENANCY_HTTP_IDENTIFICATION', 'path'),
];
