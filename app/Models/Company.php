<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Tenancy\Affects\Connections\Support\Traits\OnTenant;

use App\Support\HasMultipleMedia;

class Company extends Model
{
    use HasMultipleMedia,
        OnTenant;

    protected $fillable = [
        'name',
        'title',
        'email',
        'phone',
        'email_others',
        'phone_others',
        'website_url',
        'address',
        'billing_address',
        'tax_office',
        'tax_number',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_others' => 'array',
        'phone_others' => 'array',
    ];

    public function connections()
    {
        return $this->hasMany(CustomerConnection::class, 'company_id')
            ->orderBy('occurs_at', 'desc');
    }

    public function users()
    {
        return $this->belongsToMany(
            User::class,
            'company_users',
            'company_id',
            'user_id'
        );
    }
}
