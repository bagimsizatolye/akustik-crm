<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Tenancy\Affects\Connections\Support\Traits\OnTenant;

class Product extends Model
{
    use OnTenant;

    protected $fillable = [
        'product_category_id',
        'title',
    ];

    public function productCategory()
    {
        return $this->belongsTo(ProductCategory::class, 'product_category_id');
    }

    public function media()
    {
        return $this->hasMany(Media::class, 'related_id', 'id')
            ->relatedTo('product')
            ->orderBy('order_num');
    }

    public function mediaFeatured()
    {
        return $this->hasOne(Media::class, 'related_id', 'id')
            ->relatedTo('product')
            ->orderBy('is_featured', 'desc')
            ->orderBy('order_num');
    }

    public function prices()
    {
        return $this->hasMany(ProductPrice::class, 'product_id');
    }

    public function currentPrice()
    {
        return $this->hasOne(ProductPrice::class, 'product_id')
            ->whereNull('ends_at');
    }
}
