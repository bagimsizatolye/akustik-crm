<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

use Symfony\Component\Console\Input\InputInterface;

use Tenancy\Identification\Concerns\AllowsTenantIdentification;
use Tenancy\Identification\Contracts\Tenant as TenantContract;
use Tenancy\Identification\Drivers\Console\Contracts\IdentifiesByConsole;
use Tenancy\Identification\Drivers\Http\Contracts\IdentifiesByHttp;
use Tenancy\Tenant\Events;

class Tenant extends Model implements
    TenantContract,
    IdentifiesByHttp,
    IdentifiesByConsole
{
    use AllowsTenantIdentification;

    protected $table = 'tenants';

    protected $fillable = [
        'title',
        'host',
        'database_name',
        'owner_id',
    ];

    protected $dispatchesEvents = [
        'created' => Events\Created::class,
        'updated' => Events\Updated::class,
        'deleted' => Events\Deleted::class,
    ];

    public function getTenantKeyName(): string
    {
        \Log::info('getTenantKeyName');
        return 'database_name';
    }

    /**
     * The actual value of the key for the tenant Model.
     *
     * @return string|int
     */
    public function getTenantKey()
    {
        \Log::info('getTenantKey');
        return $this->database_name;
    }

    /**
     * A unique identifier, eg class or table to distinguish this tenant Model.
     *
     * @return string
     */
    public function getTenantIdentifier(): string
    {
        \Log::info('getTenantIdentifier');
        return get_class($this);
    }

    /**
     * Specify whether the tenant model is matching the request.
     *
     * @param Request $request
     * @return Tenant
     */
    public function tenantIdentificationByHttp(Request $request): ?Tenant
    {
        $identification = config('akustikcrm.tenancy_http_identification');

        if ($identification === 'domain') {
            return $this->query()
                ->where('host', $request->getHttpHost())
                ->first();
        } elseif ($identification === 'path') {
            return $this->query()
                ->where($this->getTenantKeyName(), $request->segment(1))
                ->first();
        }

        throw new \Exception('Invalid TENANCY_HTTP_IDENTIFICATION environment variable');
    }

    public function tenantIdentificationByConsole(InputInterface $input): ?Tenant
    {
        if ($input->hasParameterOption('--tenant')) {
            \Log::info('identify', [
                $this->getTenantKeyName(), $input->getParameterOption('--tenant')
            ]);
            return $this->query()
                ->where($this->getTenantKeyName(), $input->getParameterOption('--tenant'))
                ->first();
        }

        return null;
    }
}
