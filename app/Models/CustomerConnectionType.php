<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Tenancy\Affects\Connections\Support\Traits\OnTenant;

class CustomerConnectionType extends Model
{
    use OnTenant;

    public const tSales = 1;
    public const tFix = 2;
    public const tMaintenance = 3;
    public const tService = 4;
    public const tContact = 5;
    public const tQuotation = 6;
    public const tMeeting = 7;
    public const tNote = 8;

    protected $fillable = [
        'title',
    ];
}
