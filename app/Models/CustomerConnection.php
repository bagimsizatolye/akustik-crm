<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Tenancy\Affects\Connections\Support\Traits\OnTenant;

use App\Support\HasMultipleMedia;

class CustomerConnection extends Model
{
    use HasMultipleMedia,
        OnTenant;

    protected $fillable = [
        'customer_connection_type_id',
        'user_id',
        'company_id',
        'description',
        'price',
        'occurs_at',
        'ends_at',
    ];

    public $casts = [
        'occurs_at' => 'datetime',
        'ends_at' => 'datetime',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function products()
    {
        return $this->belongsToMany(
            Product::class,
            'customer_connection_products',
            'customer_connection_id',
            'product_id'
        );
    }

    public function type()
    {
        return $this->belongsTo(
            CustomerConnectionType::class,
            'customer_connection_type_id'
        );
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopeType($query, int $type)
    {
        return $query->where(
            'customer_connections.customer_connection_type_id',
            $type
        );
    }
}
