<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Tenancy\Affects\Connections\Support\Traits\OnTenant;

class ProductCategory extends Model
{
    use OnTenant;

    protected $fillable = [
        'title',
    ];

    public function products()
    {
        return $this->hasMany(Product::class, 'product_category_id');
    }
}
