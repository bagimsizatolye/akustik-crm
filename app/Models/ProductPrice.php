<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Tenancy\Affects\Connections\Support\Traits\OnTenant;

class ProductPrice extends Model
{
    use OnTenant;

    protected $fillable = [
        'starts_at',
        'ends_at',
        'amount',
        'product_id',
    ];

    public $casts = [
        'starts_at' => 'date',
        'ends_at' => 'date',
        'amount' => 'float',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
