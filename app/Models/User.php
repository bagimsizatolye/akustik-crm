<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Tenancy\Affects\Connections\Support\Traits\OnTenant;

class User extends Authenticatable
{
    use Notifiable,
        OnTenant;

    const tUser = 10;
    const tClient = 20;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'password',
        'email_others',
        'phone_others',
        'address',
        'billing_address',
        'tax_number',
        'tax_office',
        'government_id_number',
        'reference_user_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'email_others' => 'array',
        'phone_others' => 'array',
    ];

    public function media()
    {
        return $this->hasMany(Media::class, 'related_id', 'id')
            ->relatedTo(self::class)
            ->orderBy('order_num');
    }

    public function mediaFeatured()
    {
        return $this->hasOne(Media::class, 'related_id', 'id')
            ->relatedTo(self::class)
            ->orderBy('is_featured', 'desc')
            ->orderBy('order_num');
    }

    public function companies()
    {
        return $this->belongsToMany(
            Company::class,
            'company_users',
            'user_id',
            'company_id'
        );
    }

    public function connections()
    {
        return $this->hasMany(CustomerConnection::class, 'user_id')
            ->orderBy('occurs_at', 'desc');
    }

    public function referenceUser()
    {
        return $this->belongsTo(self::class, 'reference_user_id');
    }
}
