<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

use Tenancy\Affects\Connections\Support\Traits\OnTenant;

class Media extends Model
{
    use SoftDeletes,
        OnTenant;

    public $table = 'media';

    protected $fillable = [
        'related_id',
        'related_to',
        'title',
        'path',
        'path_thumbnail',
        'type_id',
        'height',
        'width',
    ];

    protected $hidden = [
        'deleted_at',
    ];

    public $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public $appends = [
        'url',
        'url_thumb',
    ];

    public $casts = [
        'height' => 'int',
        'width' => 'int',
    ];

    public function getUrlAttribute()
    {
        $tenant = Session::get('tenant');

        if ($this->type_id === 'image') {
            $path = (!empty($tenant) ? $tenant->getTenantKey() . '/' : '') .
                $this->path ?? $this->path_thumbnail;
            return Storage::disk('tenant')->url($path);
        } elseif ($this->type_id === 'video') {
            return Storage::disk('tenant')->url($this->path);
        }

        return Storage::disk('tenant')->url($this->path_thumbnail);
    }

    public function getUrlThumbAttribute()
    {
        $tenant = Session::get('tenant');

        $path = (!empty($tenant) ? $tenant->getTenantKey() . '/' : '') .
            $this->path_thumbnail ?? $this->path;

        return Storage::disk('tenant')->url($path);
    }

    public function getIframeCodeAttribute()
    {
        return Video::getIframe($this->path) ?: null;
    }

    public function scopeRelatedTo($query, $relatedTo)
    {
        return $query->where('related_to', $relatedTo);
    }

    public function scopeType($query, string $typeId)
    {
        return $query->where('type_id', $typeId);
    }

    public function url(string $type = 'original')
    {
        if ($type === 'original') {
            return $this->url;
        } else {
            return $this->url_thumb;
        }
    }
}
