<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\JsonResponse;

use App\Http\Controllers\Controller;
use App\Models\Media;
use App\Services\MediaService;

class MediaController extends Controller
{
    /**
     * @var MediaService
     */
    protected $mediaService;

    public function __construct(MediaService $mediaService)
    {
        $this->mediaService = $mediaService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $data = [
            'related_to' => $request->input('related_to'),
            'related_id' => $request->input('related_id'),
            'type_id' => 'image',
        ];

        $media = $this->mediaService->saveMedia(
            new Media(),
            $data,
            $request->file('image'),
            false,
            [
                'width' => $request->input('width'),
                'height' => $request->input('height'),
                'canvasWidth' => $request->input('width'),
                'canvasHeight' => $request->input('height'),
            ]
        );

        return response()->json($media);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function show(Media $media)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function edit(Media $media)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $mediaId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $mediaId)
    {
        $media = $this->mediaService->getMedia($mediaId);

        if (!$media) {
            abort(404);
        }

        $formData = $request->validate([
            'old_index' => 'sometimes|required_with:new_index|int',
            'new_index' => 'sometimes|required_with:old_index|int',
        ]);

        $media = $this->mediaService->orderMedia(
            $media,
            $formData['new_index'],
            $formData['old_index']
        );

        return response()->json($media);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function destroy($mediaId): RedirectResponse
    {
        $media = Media::find($mediaId);
        $media->delete();

        return redirect()->back()
            ->with('success', __('Silindi'));
    }
}
