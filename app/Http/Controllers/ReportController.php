<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\ReportService;

class ReportController extends Controller
{
    /**
     * @var ReportService
     */
    protected $reportService;

    public function __construct(
        ReportService $reportService
    ) {
        $this->reportService = $reportService;
    }

    public function getMonthlyIncome(Request $request)
    {
        $reportData = $this->reportService->getMonthlyIncome();

        return view('report.monthly-income', [
            'reportData' => $reportData,
        ]);
    }
}
