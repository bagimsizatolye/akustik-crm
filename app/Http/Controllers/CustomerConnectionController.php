<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\CustomerConnection;
use App\Services\CompanyService;
use App\Services\CustomerConnectionService;
use App\Services\UserService;

class CustomerConnectionController extends Controller
{
    /**
     * @var CompanyService
     */
    protected $companyService;

    /**
     * @var CustomerConnectionService
     */
    protected $customerConnectionService;

    /**
     * @var UserService
     */
    protected $userService;

    public function __construct(
        CustomerConnectionService $customerConnectionService,
        CompanyService $companyService,
        UserService $userService
    ) {
        $this->companyService = $companyService;
        $this->customerConnectionService = $customerConnectionService;
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filterData = $request->validate([
            'query' => 'sometimes|string|nullable',
        ]);

        $list = $this->customerConnectionService->getPagedList($filterData);

        return view('customer-connection.index', [
            'list' => $list,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data = $request->validate([
            'company_id' => '',
            'user_id' => '',
            'type' => '',
        ]);

        $company = null;
        $user = null;
        $type = null;

        if (\array_key_exists('company_id', $data) and !empty($data['company_id'])) {
            $company = $this->companyService->getById($data['company_id']);
        }

        if (\array_key_exists('user_id', $data) and !empty($data['user_id'])) {
            $user = $this->userService->getById($data['user_id']);
        }

        if (\array_key_exists('type', $data) and !empty($data['type'])) {
            $type = $this->customerConnectionService
                ->getConnectionTypeById($data['type']);
        }

        $connectionTypeList = $this->customerConnectionService
            ->getConnectionTypeList();

        $redirectUrl = $request->filled('redirect_url') ?
            $request->input('redirect_url') :
            null;

        return view('customer-connection.form', [
            'company' => $company,
            'type' => $type,
            'user' => $user,
            'customerConnection' => new CustomerConnection(),
            'connectionTypeList' => $connectionTypeList,
            'redirectUrl' => $redirectUrl,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formData = $request->validate([
            'customer_connection_type_id' => 'required',
            'company_id' => 'nullable',
            'user_id' => 'nullable',
            'description' => 'nullable|max:10000',
            'price' => 'nullable',
            'occurs_at_date' => '',
            'occurs_at_time' => '',
            'ends_at_date' => '',
            'ends_at_time' => '',
            'media' => 'array',
            'media.*' => 'image|mimes:jpeg,png',
            'product_ids' => 'sometimes|array',
        ]);

        if (!empty($formData['occurs_at_date']) and !empty($formData['occurs_at_time'])) {
            $formData['occurs_at'] = $formData['occurs_at_date'] .
            ' ' . $formData['occurs_at_time'] . ':00';
        }

        if (!empty($formData['ends_at_date']) and !empty($formData['ends_at_time'])) {
            $formData['ends_at'] = $formData['ends_at_date'] .
            ' ' . $formData['ends_at_time'] . ':00';
        }

        $customerConnection = $this->customerConnectionService->save(
            new CustomerConnection(),
            $formData
        );

        if ($request->filled('redirect_url')) {
            $redirect = $request->input('redirect_url');
        } else {
            $redirect = route('company.show', [$customerConnection->company_id]);
        }

        return redirect()->to($redirect)
            ->with([
                'success' => __('Kaydedildi'),
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CustomerConnection  $customerConnection
     * @return \Illuminate\Http\Response
     */
    public function show(CustomerConnection $customerConnection)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CustomerConnection  $customerConnection
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomerConnection $customerConnection)
    {
        $this->customerConnectionService->loadFormRelations($customerConnection);
        $company = $customerConnection->company;
        $user = $customerConnection->user;
        $type = $customerConnection->type;

        $connectionTypeList = $this->customerConnectionService
            ->getConnectionTypeList();

        $redirectUrl = request()->filled('redirect_url') ?
            request()->input('redirect_url') :
            null;

        return view('customer-connection.form', [
            'company' => $company,
            'type' => $type,
            'user' => $user,
            'customerConnection' => $customerConnection,
            'connectionTypeList' => $connectionTypeList,
            'redirectUrl' => $redirectUrl,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CustomerConnection  $customerConnection
     * @return \Illuminate\Http\Response
     */
    public function update(
        Request $request,
        CustomerConnection $customerConnection
    ) {
        $formData = $request->validate([
            'customer_connection_type_id' => 'required',
            'company_id' => 'nullable',
            'user_id' => 'nullable',
            'description' => 'nullable|max:10000',
            'price' => 'nullable',
            'occurs_at_date' => '',
            'occurs_at_time' => '',
            'ends_at_date' => '',
            'ends_at_time' => '',
            'media' => 'array',
            'media.*' => 'image|mimes:jpeg,png',
            'product_ids' => 'sometimes|array',
        ]);

        if (!empty($formData['occurs_at_date']) and !empty($formData['occurs_at_time'])) {
            $formData['occurs_at'] = $formData['occurs_at_date'] .
            ' ' . $formData['occurs_at_time'] . ':00';
        }

        if (!empty($formData['ends_at_date']) and !empty($formData['ends_at_time'])) {
            $formData['ends_at'] = $formData['ends_at_date'] .
            ' ' . $formData['ends_at_time'] . ':00';
        }

        $customerConnection = $this->customerConnectionService->save(
            $customerConnection,
            $formData
        );

        if ($request->filled('redirect_url')) {
            $redirect = $request->input('redirect_url');
        } else {
            $redirect = route('company.show', [$customerConnection->company_id]);
        }

        return redirect()->to($redirect)
            ->with([
                'success' => __('Kaydedildi'),
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CustomerConnection  $customerConnection
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomerConnection $customerConnection)
    {
        $status = $this->customerConnectionService->remove($customerConnection);

        return redirect()->route('customer-connection.index')
            ->with([
                'success' => __('Silindi'),
            ]);
    }
}
