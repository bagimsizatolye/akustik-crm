<?php

namespace App\Http\Controllers;

use App\Models\CustomerConnectionType;
use Illuminate\Http\Request;

class CustomerConnectionTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CustomerConnectionType  $customerConnectionType
     * @return \Illuminate\Http\Response
     */
    public function show(CustomerConnectionType $customerConnectionType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CustomerConnectionType  $customerConnectionType
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomerConnectionType $customerConnectionType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CustomerConnectionType  $customerConnectionType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomerConnectionType $customerConnectionType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CustomerConnectionType  $customerConnectionType
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomerConnectionType $customerConnectionType)
    {
        //
    }
}
