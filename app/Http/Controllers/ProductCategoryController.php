<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\ProductCategory;
use App\Services\ProductCategoryService;

class ProductCategoryController extends Controller
{
    /**
     * @var ProductCategoryService
     */
    protected $productCategoryService;

    public function __construct(ProductCategoryService $productCategoryService)
    {
        $this->productCategoryService = $productCategoryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = $this->productCategoryService->getPagedList();

        return view('product-category.index', [
            'list' => $list,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product-category.form', [
            'productCategory' => new ProductCategory(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formData = $request->validate([
            'title' => 'required|string|max:500|min:1',
        ]);

        $productCategory = $this->productCategoryService->save(
            new ProductCategory(),
            $formData
        );

        if ($request->ajax()) {
            return response()->json([
                'productCategory' => $productCategory,
            ]);
        }

        return redirect()->route('product-category.index')
            ->with([
                'success' => __('Kaydedildi'),
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ProductCategory $productCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductCategory $productCategory)
    {
        return view('product-category.form', [
            'productCategory' => $productCategory,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductCategory $productCategory)
    {
        $formData = $request->validate([
            'title' => 'required|sometimes|string|max:500|min:1',
        ]);

        $this->productCategoryService->save($productCategory, $formData);

        return redirect()->back()
            ->with([
                'success' => __('Kaydedildi'),
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductCategory $productCategory)
    {
        $status = $this->productCategoryService->remove($productCategory);

        if (!$status) {
            return redirect()->back()
                ->with([
                    'errors' => __('Kategori silinemedi. Bu kategoride ürün olabilir mi?'),
                ]);
        }

        return redirect()->route('product-category.index')
            ->with([
                'success' => __('Silindi'),
            ]);
    }
}
