<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Company;
use App\Models\Tenant;
use App\Services\CompanyService;
use App\Services\CustomerConnectionService;

class CompanyController extends Controller
{
    /**
     * @var CompanyService
     */
    protected $companyService;

    /**
     * @var CustomerConnectionService
     */
    protected $customerConnectionService;

    public function __construct(
        CompanyService $companyService,
        CustomerConnectionService $customerConnectionService
    ) {
        $this->companyService = $companyService;
        $this->customerConnectionService = $customerConnectionService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filterData = $request->validate([
            'query' => 'sometimes|string|nullable',
        ]);

        $list = $this->companyService->getPagedList($filterData);

        return view('company.index', [
            'list' => $list,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company.form', [
            'company' => new Company(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formData = $request->validate([
            'name' => 'required|string|max:500|min:1',
            'title' => 'nullable|string|max:500|min:1',
            'email' => 'nullable|email',
            'phone' => 'nullable|string|min:5|max:20',
            'email_others' => 'array',
            'email.*' => 'email',
            'phone_others' => 'array',
            'phone_others.*' => 'string|min:5|max:20',
            'media' => 'array',
            'media.*' => 'image|mimes:jpeg,png',
            'website_url' => 'nullable|url|max:500|min:10',
            'address' => 'nullable|sometimes|max:2000',
            'billing_address' => 'nullable|sometimes|max:2000',
            'tax_number' => 'nullable|sometimes|max:100',
            'tax_office' => 'nullable|sometimes|max:100',
            'user_ids' => 'sometimes|array',
        ]);

        $this->companyService->save(new Company(), $formData);

        return redirect()->route('company.index')
            ->with([
                'success' => __('Kaydedildi'),
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tenant  $tenant
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Tenant $tenant = null, Company $company)
    {
        $company->load([
            'mediaFeatured',
            'media',
            'connections',
            'connections.user',
        ]);

        $connectionTypeList = $this->customerConnectionService->getConnectionTypeList();

        return view('company.show', [
            'company' => $company,
            'connectionTypeList' => $connectionTypeList,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('company.form', [
            'company' => $company,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $formData = $request->validate([
            'name' => 'sometimes|string|max:500|min:1',
            'title' => 'nullable|sometimes|string|max:500|min:1',
            'email' => 'nullable|sometimes|email',
            'phone' => 'nullable|sometimes|string|min:5|max:20',
            'email_others' => 'sometimes|array',
            'email.*' => 'sometimes|email',
            'phone_others' => 'sometimes|array',
            'phone_others.*' => 'sometimes|string|min:5|max:20',
            'media' => 'sometimes|array',
            'media.*' => 'sometimes|image|mimes:jpeg,png',
            'website_url' => 'nullable|sometimes|url|max:500|min:10',
            'address' => 'nullable|sometimes|max:2000',
            'billing_address' => 'nullable|sometimes|max:2000',
            'tax_number' => 'nullable|sometimes|max:100',
            'tax_office' => 'nullable|sometimes|max:100',
            'user_ids' => 'sometimes|array',
        ]);

        $this->companyService->save($company, $formData);

        return redirect()->back()
            ->with([
                'success' => __('Kaydedildi'),
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $status = $this->companyService->remove($company);

        return redirect()->route('company.index')
            ->with([
                'success' => __('Silindi'),
            ]);
    }
}
