<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Tenant;
use App\Services\CustomerConnectionService;
use App\Services\UserService;

class UserController extends Controller
{
    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @var CustomerConnectionService
     */
    protected $customerConnectionService;

    public function __construct(
        CustomerConnectionService $customerConnectionService,
        UserService $userService
    ) {
        $this->customerConnectionService = $customerConnectionService;
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filterData = $request->validate([
            'query' => 'sometimes|string|nullable',
        ]);

        $list = $this->userService->getPagedList($filterData);

        return view('user.index', [
            'list' => $list,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.form', [
            'user' => new User(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formData = $request->validate([
            'name' => 'required|string|max:500|min:1',
            'phone' => 'nullable|string|max:20|min:5',
            'email' => 'nullable|email',
            'media' => 'array',
            'media.*' => 'image|mimes:jpeg,png',
            'company_ids' => 'sometimes|array',
            'address' => 'nullable|max:2000',
            'billing_address' => 'nullable|max:2000',
            'tax_number' => 'nullable|max:100',
            'tax_office' => 'nullable|max:100',
            'government_id_number' => 'nullable',
            'reference_user_id' => 'nullable',
        ]);

        $this->userService->save(new User(), $formData);

        return redirect()->route('user.index')
            ->with([
                'success' => __('Kaydedildi'),
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(Tenant $tenant = null, User $user)
    {
        $user->load([
            'mediaFeatured',
            'media',
            'connections',
            'connections.company',
            'referenceUser',
        ]);

        $connectionTypeList = $this->customerConnectionService->getConnectionTypeList();

        return view('user.show', [
            'user' => $user,
            'connectionTypeList' => $connectionTypeList,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(Tenant $tenant = null, User $user)
    {
        $user->load([
            'media',
            'companies',
            'referenceUser',
        ]);

        return view('user.form', [
            'user' => $user,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Tenant $tenant = null, Request $request, User $user)
    {
        $formData = $request->validate([
            'name' => 'required|string|max:500|min:1',
            'phone' => 'sometimes|nullable|string|max:20|min:5',
            'email' => 'sometimes|nullable|email',
            'media' => 'sometimes|array',
            'media.*' => 'sometimes|image|mimes:jpeg,png',
            'company_ids' => 'sometimes|array',
            'address' => 'nullable|sometimes|max:2000',
            'billing_address' => 'nullable|sometimes|max:2000',
            'tax_number' => 'nullable|sometimes|max:100',
            'tax_office' => 'nullable|sometimes|max:100',
            'government_id_number' => 'nullable|sometimes',
            'reference_user_id' => 'nullable|sometimes',
        ]);

        $this->userService->save($user, $formData);

        return redirect()->back()
            ->with([
                'success' => __('Kaydedildi'),
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if ($user->id === auth()->id()) {
            return redirect()->back()
                ->with([
                    'errors' => __('Kendinizi silemezsiniz'),
                ]);
        }

        $status = $this->userService->remove($user);

        if (!$status) {
            return redirect()->back()
                ->with([
                    'errors' => __('Kişi silinemedi.'),
                ]);
        }

        return redirect()->route('user.index')
            ->with([
                'success' => __('Silindi'),
            ]);
    }
}
