<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Product;
use App\Services\ProductCategoryService;
use App\Services\ProductService;

class ProductController extends Controller
{
    /**
     * @var ProductService
     */
    protected $productService;

    /**
     * @var ProductCategoryService
     */
    protected $productCategoryService;

    public function __construct(
        ProductService $productService,
        ProductCategoryService $productCategoryService
    ) {
        $this->productService = $productService;
        $this->productCategoryService = $productCategoryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filterData = $request->validate([
            'product_category_id' => 'sometimes|int|nullable',
            'title' => 'sometimes|string|nullable',
        ]);

        $categoryList = $this->productCategoryService->getList();
        $list = $this->productService->getPagedList($filterData);

        return view('product.index', [
            'list' => $list,
            'categoryList' => $categoryList,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoryList = $this->productCategoryService->getList();

        return view('product.form', [
            'product' => new Product(),
            'categoryList' => $categoryList,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formData = $request->validate([
            'title' => 'required|string|max:500|min:1',
            'product_category_id' => 'required|int',
            'price_amount' => 'numeric|min:0|max:99999999',
            'media' => 'array',
            'media.*' => 'image|mimes:jpeg,png',
        ]);

        $this->productService->save(new Product(), $formData);

        return redirect()->route('product.index')
            ->with([
                'success' => __('Kaydedildi'),
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categoryList = $this->productCategoryService->getList();

        $product->load([
            'media',
        ]);

        return view('product.form', [
            'product' => $product,
            'categoryList' => $categoryList,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $formData = $request->validate([
            'title' => 'required|string|max:500|min:1',
            'product_category_id' => 'required|int',
            'price_amount' => 'numeric|min:0|max:99999999',
            'media' => 'sometimes|array',
            'media.*' => 'sometimes|image|mimes:jpeg,png',
        ]);

        $this->productService->save($product, $formData);

        return redirect()->back()
            ->with([
                'success' => __('Kaydedildi'),
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $status = $this->productService->remove($product);

        if (!$status) {
            return redirect()->back()
                ->with([
                    'errors' => __('Ürün silinemedi.'),
                ]);
        }

        return redirect()->route('product.index')
            ->with([
                'success' => __('Silindi'),
            ]);
    }
}
