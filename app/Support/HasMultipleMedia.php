<?php

namespace App\Support;

use App\Models\Media;

/**
 * Adds media and mediaFeatured relations to Eloquent models.
 */
trait HasMultipleMedia
{
    public function media()
    {
        return $this->hasMany(Media::class, 'related_id', 'id')
            ->relatedTo(self::class)
            ->orderBy('order_num');
    }

    public function mediaFeatured()
    {
        return $this->hasOne(Media::class, 'related_id', 'id')
            ->relatedTo(self::class)
            ->orderBy('is_featured', 'desc')
            ->orderBy('order_num');
    }
}
