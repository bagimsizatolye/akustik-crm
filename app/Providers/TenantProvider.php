<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Tenancy\Identification\Contracts\ResolvesTenants;

use App\Models\Tenant;

class TenantProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        \Log::info('TenantProvider-register');
        $this->app->resolving(
            ResolvesTenants::class,
            function (ResolvesTenants $resolver) {
                \Log::info('TenantProvider-closure');
                $resolver->addModel(Tenant::class);

                return $resolver;
            }
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
