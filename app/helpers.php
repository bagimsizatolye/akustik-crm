<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;

use Carbon\Carbon;

use App\Services\TextService;

function format_datetime(Carbon $carbon, $format = '%e %b %Y %H:%M'): string
{
    return $carbon->locale('tr')->formatLocalized($format);
}

function format_date(Carbon $carbon, $format = 'D MMMM YYYY'): string
{
    return $carbon->locale(app()->getLocale())->isoFormat($format);
}

function set_locale(string $locale)
{
    Carbon::setLocale($locale);
    App::setLocale($locale);

    if ($locale === 'tr') {
        setlocale(LC_TIME, 'tr_TR.utf8');
    } else {
        setlocale(LC_TIME, 'en_US.utf8');
    }

    Cookie::queue('locale', $locale, 60 * 24 * 30);

    return $locale;
}
