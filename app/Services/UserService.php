<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Models\Media;
use App\Services\MediaService;
use App\Models\User;

class UserService
{
    /**
     * @var MediaService
     */
    protected $mediaService;

    public function __construct(MediaService $mediaService)
    {
        $this->mediaService = $mediaService;
    }

    public function getById(int $id): ?User
    {
        return User::find($id);
    }

    public function getPagedList($filterData = [], $pageLength = 30): LengthAwarePaginator
    {
        $query = User::orderBy('name')
            ->with([
                'mediaFeatured',
            ]);

        if (\array_key_exists('query', $filterData) and !empty($filterData['query'])) {
            $query->where('name', 'like', '%' . $filterData['query'] . '%')
                ->orWhere('email', 'like', '%' . $filterData['query'] . '%')
                ->orWhere('phone', 'like', '%' . $filterData['query'] . '%');
        }

        if (\array_key_exists('company_id', $filterData) and !empty($filterData['company_id'])) {
            // TODO: add company_id filter
        }

        return $query->paginate($pageLength);
    }

    public function save(User $model, array $formData): User
    {
        DB::beginTransaction();

        if (\array_key_exists('name', $formData) and is_string($formData['name'])) {
            $model->name = $formData['name'];
        }

        if (\array_key_exists('email', $formData) and is_string($formData['email'])) {
            $model->email = $formData['email'];
        }

        if (\array_key_exists('phone', $formData) and is_string($formData['phone'])) {
            $model->phone = $formData['phone'];
        }

        if (\array_key_exists('address', $formData) and is_string($formData['address'])) {
            $model->address = $formData['address'];
        }

        if (\array_key_exists('billing_address', $formData) and is_string($formData['billing_address'])) {
            $model->billing_address = $formData['billing_address'];
        }

        if (\array_key_exists('tax_number', $formData) and is_string($formData['tax_number'])) {
            $model->tax_number = $formData['tax_number'];
        }

        if (\array_key_exists('tax_office', $formData) and is_string($formData['tax_office'])) {
            $model->tax_office = $formData['tax_office'];
        }

        if (\array_key_exists('reference_user_id', $formData) and is_string($formData['reference_user_id'])) {
            $model->reference_user_id = $formData['reference_user_id'];
        }

        if (\array_key_exists('government_id_number', $formData) and is_string($formData['government_id_number'])) {
            $model->government_id_number = $formData['government_id_number'];
        }

        $model->save();

        if (\array_key_exists('company_ids', $formData) and is_array($formData['company_ids'])) {
            $model->companies()->sync($formData['company_ids']);
        }

        if (\array_key_exists('media', $formData) and !empty($formData['media'])) {
            $imageData = [
                'type_id' => 'image',
                'related_to' => User::class,
                'related_id' => $model->id,
            ];

            $mediaIds = [];

            foreach ($formData['media'] as $item) {
                $media = $this->mediaService->saveMedia(
                    new Media(),
                    $imageData,
                    $item,
                    [
                        'width' => 600,
                        'height' => 400,
                    ],
                    [
                        'width' => 1200,
                        'height' => null,
                        'canvasWidth' => 1200,
                        'canvasHeight' => 800,
                    ]
                );

                $mediaIds[] = $media->id;
            }

            $model->load('media');
        }

        DB::commit();

        return $model;
    }

    public function remove(User $model): ?bool
    {
        return $model->delete();
    }
}
