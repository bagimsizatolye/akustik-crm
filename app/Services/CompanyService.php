<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Models\Company;
use App\Models\Media;
use App\Services\MediaService;

class CompanyService
{
    /**
     * @var MediaService
     */
    protected $mediaService;

    public function __construct(MediaService $mediaService)
    {
        $this->mediaService = $mediaService;
    }

    public function getById(int $id): ?Company
    {
        return Company::find($id);
    }

    public function getPagedList($filterData = [], $pageLength = 30): LengthAwarePaginator
    {
        $query = Company::orderBy('name')
            ->with([
                'mediaFeatured',
            ]);

        if (\array_key_exists('query', $filterData) and !empty($filterData['query'])) {
            $query->where('name', 'like', '%' . $filterData['query'] . '%')
                ->orWhere('title', 'like', '%' . $filterData['query'] . '%')
                ->orWhere('email', 'like', '%' . $filterData['query'] . '%')
                ->orWhere('phone', 'like', '%' . $filterData['query'] . '%');
        }

        return $query->paginate($pageLength);
    }

    public function save(Company $model, array $formData): Company
    {
        DB::beginTransaction();

        if (\array_key_exists('name', $formData) and is_string($formData['name'])) {
            $model->name = $formData['name'];
        }

        if (\array_key_exists('email', $formData) and is_string($formData['email'])) {
            $model->email = $formData['email'];
        }

        if (\array_key_exists('phone', $formData) and is_string($formData['phone'])) {
            $model->phone = $formData['phone'];
        }

        if (\array_key_exists('title', $formData) and is_string($formData['title'])) {
            $model->title = $formData['title'];
        }

        if (\array_key_exists('website_url', $formData) and is_string($formData['website_url'])) {
            $model->website_url = $formData['website_url'];
        }

        if (\array_key_exists('address', $formData) and is_string($formData['address'])) {
            $model->address = $formData['address'];
        }

        if (\array_key_exists('billing_address', $formData) and is_string($formData['billing_address'])) {
            $model->billing_address = $formData['billing_address'];
        }

        if (\array_key_exists('tax_number', $formData) and is_string($formData['tax_number'])) {
            $model->tax_number = $formData['tax_number'];
        }

        if (\array_key_exists('tax_office', $formData) and is_string($formData['tax_office'])) {
            $model->tax_office = $formData['tax_office'];
        }

        $model->save();

        if (\array_key_exists('user_ids', $formData) and is_array($formData['user_ids'])) {
            $model->users()->sync($formData['user_ids']);
        }

        if (\array_key_exists('media', $formData) and !empty($formData['media'])) {
            $imageData = [
                'type_id' => 'image',
                'related_to' => Company::class,
                'related_id' => $model->id,
            ];

            $mediaIds = [];

            foreach ($formData['media'] as $item) {
                $media = $this->mediaService->saveMedia(
                    new Media(),
                    $imageData,
                    $item,
                    [
                        'width' => 600,
                        'height' => 400,
                    ],
                    [
                        'width' => 1200,
                        'height' => null,
                        'canvasWidth' => 1200,
                        'canvasHeight' => 800,
                    ]
                );

                $mediaIds[] = $media->id;
            }

            $model->load('media');
        }

        DB::commit();

        return $model;
    }

    public function remove(Company $model): ?bool
    {
        return $model->delete();
    }
}
