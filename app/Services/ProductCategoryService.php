<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Models\Product;
use App\Models\ProductCategory;

class ProductCategoryService
{
    public function getPagedList($pageLength = 30): LengthAwarePaginator
    {
        return ProductCategory::orderBy('title')
            ->paginate($pageLength);
    }

    public function getList(): Collection
    {
        return ProductCategory::orderBy('title')
            ->get();
    }

    public function save(ProductCategory $model, array $formData): ProductCategory
    {
        if(\array_key_exists('title', $formData) and is_string($formData['title'])) {
            $model->title = $formData['title'];
        }

        $model->save();

        return $model;
    }

    public function remove(ProductCategory $model): ?bool
    {
        $productExists = Product::where('product_category_id', $model->id)->exists();

        if($productExists) {
            return false;
        }

        return $model->delete();
    }
}
