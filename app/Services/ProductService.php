<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

use Carbon\Carbon;

use App\Models\Media;
use App\Services\MediaService;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductPrice;

class ProductService
{
    /**
     * @var MediaService
     */
    protected $mediaService;

    public function __construct(MediaService $mediaService)
    {
        $this->mediaService = $mediaService;
    }

    public function getPagedList($filterData = [], $pageLength = 30): LengthAwarePaginator
    {
        $query = Product::orderBy('title')
            ->with([
                'productCategory',
                'mediaFeatured',
            ]);

        if (\array_key_exists('product_category_id', $filterData) and !empty($filterData['product_category_id'])) {
            $query->where('product_category_id', (int)$filterData['product_category_id']);
        }

        if (\array_key_exists('title', $filterData) and !empty($filterData['title'])) {
            $query->where('title', 'like', '%' . $filterData['title'] . '%');
        }

        if (\array_key_exists('query', $filterData) and !empty($filterData['query'])) {
            $query->where('query', 'like', '%' . $filterData['query'] . '%');
        }

        return $query->paginate($pageLength);
    }

    public function save(Product $model, array $formData): Product
    {
        DB::beginTransaction();

        if (\array_key_exists('title', $formData) and is_string($formData['title'])) {
            $model->title = $formData['title'];
        }

        if (\array_key_exists('product_category_id', $formData) and is_string($formData['product_category_id'])) {
            $model->product_category_id = $formData['product_category_id'];
        }

        $model->save();

        if (\array_key_exists('media', $formData) and !empty($formData['media'])) {
            $imageData = [
                'type_id' => 'image',
                'related_to' => 'product',
                'related_id' => $model->id,
            ];

            $mediaIds = [];

            foreach ($formData['media'] as $item) {
                $media = $this->mediaService->saveMedia(
                    new Media(),
                    $imageData,
                    $item,
                    [
                        'width' => 600,
                        'height' => 400,
                    ],
                    [
                        'width' => 1200,
                        'height' => null,
                        'canvasWidth' => 1200,
                        'canvasHeight' => 800,
                    ]
                );

                $mediaIds[] = $media->id;
            }

            $model->load('media');
        }


        if (\array_key_exists('price_amount', $formData)) {
            $model->load('currentPrice');

            $newPrice = (float)$formData['price_amount'];

            if (!$model->currentPrice or $model->currentPrice->amount !== $newPrice) {
                if ($model->currentPrice) {
                    if ($model->currentPrice->starts_at->equalTo(Carbon::create(date('Y-m-d')))) {
                        $model->currentPrice->delete();
                    } else {
                        $model->currentPrice->ends_at = now();
                        $model->currentPrice->save();
                    }
                }

                ProductPrice::create([
                    'product_id' => $model->id,
                    'starts_at' => now(),
                    'amount' => $newPrice,
                ]);
            }

            $model->load('currentPrice');
        }

        DB::commit();

        return $model;
    }

    public function remove(Product $model): ?bool
    {
        return $model->delete();
    }
}
