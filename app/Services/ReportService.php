<?php

namespace App\Services;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;

use App\Models\CustomerConnection;

class ReportService
{
    public function getMonthlyIncome()
    {
        return DB::connection('tenant')
            ->table('customer_connections AS cc')
            ->selectRaw('
                SUM(cc.price) AS total_price,
                CONCAT(YEAR(occurs_at), "-", MONTH(occurs_at)) AS occurs_on,
                YEAR(occurs_at) AS occurs_year,
                MONTH(occurs_at) AS occurs_month
            ')
            ->whereNotNull('occurs_at')
            ->groupBy('occurs_on')
            ->orderBy('occurs_on', 'desc')
            ->get();
    }
}
