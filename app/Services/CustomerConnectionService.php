<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Models\CustomerConnection;
use App\Models\CustomerConnectionType;
use App\Models\Media;
use App\Services\MediaService;

class CustomerConnectionService
{
    /**
     * @var MediaService
     */
    protected $mediaService;

    public function __construct(MediaService $mediaService)
    {
        $this->mediaService = $mediaService;
    }

    public function getConnectionTypeList(): Collection
    {
        return CustomerConnectionType::orderBy('title')->get();
    }

    public function getConnectionTypeById(int $id): ?CustomerConnectionType
    {
        return CustomerConnectionType::find($id);
    }

    public function getPagedList($filterData = [], $pageLength = 30): LengthAwarePaginator
    {
        $query = CustomerConnection::orderBy('occurs_at', 'desc')
            ->with([
                'mediaFeatured',
                'user',
                'company',
                'type',
            ]);

        // if (\array_key_exists('query', $filterData) and !empty($filterData['query'])) {
        //     $query->where('name', 'like', '%' . $filterData['query'] . '%')
        //         ->orWhere('title', 'like', '%' . $filterData['query'] . '%')
        //         ->orWhere('email', 'like', '%' . $filterData['query'] . '%')
        //         ->orWhere('phone', 'like', '%' . $filterData['query'] . '%');
        // }

        return $query->paginate($pageLength);
    }

    public function save(
        CustomerConnection $model,
        array $formData
    ): CustomerConnection {
        DB::beginTransaction();

        if (\array_key_exists('description', $formData) and is_string($formData['description'])) {
            $model->description = $formData['description'];
        }

        if (\array_key_exists('customer_connection_type_id', $formData) and !empty($formData['customer_connection_type_id'])) {
            $model->customer_connection_type_id = (int)$formData['customer_connection_type_id'];
        }

        if (\array_key_exists('user_id', $formData)) {
            $model->user_id = $formData['user_id'];
        }

        if (\array_key_exists('company_id', $formData)) {
            $model->company_id = $formData['company_id'];
        }

        if (\array_key_exists('price', $formData)) {
            $model->price = $formData['price'];
        }

        if (\array_key_exists('occurs_at', $formData)) {
            $model->occurs_at = date('Y-m-d H:i:s', strtotime($formData['occurs_at']));
        }

        if (\array_key_exists('ends_at', $formData)) {
            $model->ends_at = date('Y-m-d H:i:s', strtotime($formData['ends_at']));
        }

        $model->save();

        if (\array_key_exists('product_ids', $formData) and is_array($formData['product_ids'])) {
            $model->products()->sync($formData['product_ids']);
        }

        if (\array_key_exists('media', $formData) and !empty($formData['media'])) {
            $imageData = [
                'type_id' => 'image',
                'related_to' => CustomerConnection::class,
                'related_id' => $model->id,
            ];

            $mediaIds = [];

            foreach ($formData['media'] as $item) {
                $media = $this->mediaService->saveMedia(
                    new Media(),
                    $imageData,
                    $item,
                    [
                        'width' => 600,
                        'height' => 400,
                    ],
                    [
                        'width' => 1200,
                        'height' => null,
                        'canvasWidth' => 1200,
                        'canvasHeight' => 800,
                    ]
                );

                $mediaIds[] = $media->id;
            }

            $model->load('media');
        }

        DB::commit();

        return $model;
    }

    public function loadFormRelations(CustomerConnection &$model): self
    {
        $model->load([
            'user',
            'company',
            'type',
            'products',
        ]);

        return $this;
    }

    public function remove(CustomerConnection $model): ?bool
    {
        return $model->delete();
    }
}
