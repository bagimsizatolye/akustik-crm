<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Tenant;

class CreateFirstTenant extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'akustik:create-first-tenant';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tenant = Tenant::create([
            'title' => 'Bağımsız Atölye6',
            'host' => 'bagimsizatolye6',
            'database_name' => 'akustik_bagimsizatolye6',
            'owner_id' => 1,
        ]);
    }
}
