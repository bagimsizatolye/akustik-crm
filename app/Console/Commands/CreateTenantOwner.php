<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\Models\User;

class CreateTenantOwner extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'akustik:create-tenant-owner {--name=} {--password=} {--email=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = new User();
        $user->name = $this->option('name');
        $user->password = Hash::make($this->option('password'));
        $user->email = $this->option('email');
        $user->api_token = \strtolower(Str::random(80));
        $user->save();
        $this->line(json_encode($user->toArray()));
    }
}
