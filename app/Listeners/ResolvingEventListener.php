<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

use Tenancy\Affects\Connections\Contracts\ProvidesConfiguration;
use Tenancy\Affects\Connections\Events\Drivers\Configuring;
use Tenancy\Affects\Connections\Events\Resolving;
use Tenancy\Identification\Contracts\Tenant as TenantContract;

// use Illuminate\Contracts\Queue\ShouldQueue;
// use Illuminate\Queue\InteractsWithQueue;

class ResolvingEventListener implements ProvidesConfiguration
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Resolving  $event
     * @return void
     */
    public function handle(Resolving $event)
    {
        if ($event->tenant) {
            Session::put('tenant', $event->tenant);
            URL::defaults(['tenant' => $event->tenant->getTenantKey()]);
        }

        \Log::info('ResolvingEventListener-handle', [$event->tenant, $event]);

        return $this;
    }

    public function configure(TenantContract $tenant): array
    {
        \Log::info('ResolvingEventListener-configure');
        // dd($tenant);
        $config = [];

        event(new Configuring($tenant, $config, $this));

        return $config;
    }
}
