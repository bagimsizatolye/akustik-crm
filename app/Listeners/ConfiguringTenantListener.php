<?php

namespace App\Listeners;

use Tenancy\Affects\Connections\Events\Drivers\Configuring;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ConfiguringTenantListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        \Log::info('ConfiguringTenantListener__construct');
    }

    /**
     * Handle the event.
     *
     * @param  Configuring  $event
     * @return void
     */
    public function handle(Configuring $event)
    {
        \Log::info('ConfiguringTenantListener');
        // dd($event, 'ConfiguringTenant');
        $event->useConnection('mysql', $event->defaults($event->tenant));
    }
}
