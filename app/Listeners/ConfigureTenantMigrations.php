<?php

namespace App\Listeners;

use \Tenancy\Hooks\Migration\Events\ConfigureMigrations;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ConfigureTenantMigrations
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ConfigureMigrations  $event
     * @return void
     */
    public function handle(ConfigureMigrations $event)
    {
        \Log::info('ConfigureTenantMigrations');
        $event->path(database_path('migrations/tenant'));
    }
}
