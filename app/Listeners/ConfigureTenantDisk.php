<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use Tenancy\Affects\Filesystems\Events\ConfigureDisk;

class ConfigureTenantDisk
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ConfigureDisk  $event
     * @return void
     */
    public function handle(ConfigureDisk $event)
    {
        if ($event->event->tenant) {
            $directory = storage_path('app/' . $event->event->tenant->getTenantKey());
            $directoryPublic = storage_path('app/public/' . $event->event->tenant->getTenantKey());

            if (!\file_exists($directory)) {
                \mkdir($directory, 0755);
            }

            if (!\file_exists($directoryPublic)) {
                \mkdir($directoryPublic, 0755);
                \mkdir($directoryPublic . '/images', 0755);
            }

            $event->config = [
                'driver' => 'local',
                'root' => $directoryPublic,
                'url' => '/storage',
            ];

            \config('filesystems.default', 'tenant');
        } else {
            // dd('no tenant');
        }
    }
}
