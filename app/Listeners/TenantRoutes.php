<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Tenancy\Affects\Routes\Events\ConfigureRoutes;

class TenantRoutes
{
    protected $namespace = 'App\Http\Controllers';

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ConfigureRoutes  $event
     * @return void
     */
    public function handle(ConfigureRoutes $event)
    {
        if ($event->event->tenant) {
            $identification = config('akustikcrm.tenancy_http_identification');

            if ($identification === 'domain') {
                $event->flush()
                    ->fromFile(
                        [
                            'middleware' => ['api'],
                            'namespace' => $this->namespace,
                            'prefix' => 'api',
                        ],
                        base_path('/routes/tenant-api.php')
                    )
                    ->fromFile(
                        [
                            'middleware' => ['web'],
                            'namespace' => $this->namespace,
                        ],
                        base_path('/routes/tenant.php')
                    );
            } else {
                $event->flush()
                    ->fromFile(
                        [
                            'middleware' => ['api'],
                            'namespace' => $this->namespace,
                            'prefix' => '{tenant}/api',
                        ],
                        base_path('/routes/tenant-api.php')
                    )
                    ->fromFile(
                        [
                            'middleware' => ['web'],
                            'namespace' => $this->namespace,
                            'prefix' => '{tenant}',
                        ],
                        base_path('/routes/tenant.php')
                    );
            }
        }
    }
}
