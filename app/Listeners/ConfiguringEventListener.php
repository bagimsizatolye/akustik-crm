<?php

namespace App\Listeners;

use Tenancy\Hooks\Database\Events\Drivers\Configuring;

// use Illuminate\Contracts\Queue\ShouldQueue;
// use Illuminate\Queue\InteractsWithQueue;

class ConfiguringEventListener
{
    /**
     * Handle the event.
     *
     * @param  Configuring  $event
     * @return void
     */
    public function handle(Configuring $event)
    {
        \Log::info('ConfiguringEventListener');
        // dd($event, 'ConfiguringEvent');
        $event->useConnection('mysql', $event->defaults($event->tenant));
    }
}
